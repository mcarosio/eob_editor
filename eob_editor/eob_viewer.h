#pragma once

#include "ninjalib/binary_utils.h"

namespace eob
{
	class eob_viewer
	{
	public:
		eob_viewer(const char *path);
		~eob_viewer();

	protected:
		utils::binary::buffer	m_exeData;
	};
}