#include "eob_viewer.h"

#include <filesystem>

eob::eob_viewer::eob_viewer(const char *path)
{
	std::experimental::filesystem::path p(path);
	m_exeData.load(p);
}

eob::eob_viewer::~eob_viewer()
{
}

/*
<ufwb version="1.9">
	<grammar name="EOB SAV" start="id:135" author="fedo" fileextension="SAV" complete="yes">
		<description>Grammar for EOBDATA.SAV files</description>
		<structure name="SAV file" id="135" length="0" encoding="ISO_8859-1:1987" endian="big" signed="no">
			<structure name="Character" id="136" length="243" repeatmax="6">
				<binary name="<Binary Fill Bytes>" id="137" unused="yes" length="2"/>
				<string name="NAme" id="138" fillcolor="FFC2B8" type="fixed-length" length="10" encoding="ISO_8859-1:1987"/>
				<binary name="<Binary Fill Bytes-1>" id="139" unused="yes" length="1"/>
				<number name="curr_STR" id="140" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_STR" id="141" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_adv_STR" id="142" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_adv_STR" id="143" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_INT" id="144" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_INT" id="145" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_WIS" id="146" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_WIS" id="147" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_DEX" id="148" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_DEX" id="149" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_CON" id="150" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_CON" id="151" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_CHA" id="152" fillcolor="9DFB9F" type="integer" length="1"/>
				<number name="max_CHA" id="153" fillcolor="00F900" type="integer" length="1"/>
				<number name="curr_HP" id="154" fillcolor="BEE9FF" type="integer" length="1"/>
				<number name="max_HP" id="155" fillcolor="BAD1FF" type="integer" length="1"/>
				<binary name="<Binary Fill Bytes-2>" id="156" unused="yes" length="2"/>
				<binary name="race+sex" id="157" fillcolor="FFF8BE" length="1"/>
				<binary name="class" id="158" fillcolor="FEFA22" length="1"/>
				<number name="alignment" id="159" fillcolor="FFD487" type="integer" length="1" display="hex"/>
				<number name="portrait" id="160" fillcolor="FFC1D7" type="integer" length="1" display="hex"/>
				<number name="food in % (0..100)" id="161" fillcolor="00FCF7" type="integer" length="1"/>
				<number name="Level 1st class" id="162" fillcolor="FF333C" type="integer" length="1"/>
				<number name="Level 2nd class" id="163" fillcolor="FF3A3D" type="integer" length="1"/>
				<number name="Level 3rd class" id="164" fillcolor="FF3A3D" type="integer" length="1"/>
				<number name="XP 1st class" id="165" fillcolor="FFB8FD" type="integer" length="4" endian="little"/>
				<number name="XP 2nd class" id="166" fillcolor="FFB8FD" type="integer" length="4" endian="little"/>
				<number name="XP 3rd class" id="167" fillcolor="FFB8FD" type="integer" length="4"/>
				<binary name="<Binary Fill Bytes-3>" id="168" unused="yes" length="68"/>
				<number name="lefthand slot" id="169" fillcolor="AFFA3B" type="integer" length="2" display="hex"/>
				<number name="righthand slot" id="170" fillcolor="AFFA3B" type="integer" length="2" display="hex"/>
				<binary name="Items general slot" id="171" fillcolor="B4DFFF" length="28"/>
			</structure>
		</structure>
	</grammar>
</ufwb>

*/

/*#include <stdio.h>
#include "matching.h"
#include "str_util.h"
#include "bvh.h"

unsigned char INVULNER_SEQ[] =	{0x2a,0x46,0x08,0x26,0x88,0x47,0x1b};
unsigned char INVULNER_REP[] =	{0x2a,0x46,0x08,0x90,0x90,0x90,0x90};
int INVULNER_LEN =		7;

unsigned char WTHROUGH_SEQ[] =	{0xa9,0x01,0x00,0x75,0x05};
unsigned char WTHROUGH_REP[] =	{0xa9,0x01,0x00,0xeb,0x05};
int WTHROUGH_LEN =		5;

unsigned char SPWEAPON_SEQ[] =	{0x8b,0x46,0x0a,0x26,0x29};
unsigned char SPWEAPON_REP[] =	{0x33,0xc0,0x90,0x26,0x89};
int SPWEAPON_LEN =		5;

void print_pattern(char *ptn, int len) {
	int i;
	unsigned char c;

	c = ptn[0];
	printf("%02x",c);
	for (i=1; i<len; i++) {
		c = ptn[i];
		printf(" %02x",c);
	}
}

char* copy_pattern(char *ptn, int len) {
	unsigned char *ret;
	int i;

	ret = (char*)malloc(sizeof(char) * (len+1));
	for (i=0; i<len; i++)
		ret[i] = ptn[i];
	ret[len] = '\0';
	return ret;
}

int get_int() {
	char *line;
	int i;

	read_line(&line);
	i = str2int(line);
	free(line);
	return i;
}

void invulnerability(char *arg) {
	long *lambda = NULL;
	long *gamma = NULL;
	char *pattern = NULL;
	char *sub = NULL;
	long pattern_len;
	int pos = 0;
	FILE *fp;

	printf("Invulnerabilita'\n");
	pattern_len = INVULNER_LEN;
	lambda = last_occurrence(INVULNER_SEQ,pattern_len);
	gamma = good_suffix(INVULNER_SEQ,pattern_len);
	pattern = copy_pattern(INVULNER_SEQ,pattern_len);
	sub = copy_pattern(INVULNER_REP,pattern_len);
	if ((fp = fopen(arg,"r+")) != NULL) {
		while (pos != -1)
			pos = replace(pattern,pattern_len,sub,fp,arg,lambda,gamma);
		fclose(fp);
	} else
		printf("Non trovo il file %s\n",arg);
	free(pattern);
	free(sub);
	free(lambda);
	free(gamma);
}

void walk_through(char *arg) {
	long *lambda = NULL;
	long *gamma = NULL;
	char *pattern = NULL;
	char *sub = NULL;
	long pattern_len;
	int pos = 0;
	FILE *fp;

	printf("Passa parete\n");
	pattern_len = WTHROUGH_LEN;
	lambda = last_occurrence(WTHROUGH_SEQ,pattern_len);
	gamma = good_suffix(WTHROUGH_SEQ,pattern_len);
	pattern = copy_pattern(WTHROUGH_SEQ,pattern_len);
	sub = copy_pattern(WTHROUGH_REP,pattern_len);
	if ((fp = fopen(arg,"r+")) != NULL) {
		while (pos != -1)
			pos = replace(pattern,pattern_len,sub,fp,arg,lambda,gamma);
		fclose(fp);
	} else
		printf("Non trovo il file %s\n",arg);
	free(pattern);
	free(sub);
	free(lambda);
	free(gamma);
}

void super_weapon(char *arg) {
	long *lambda = NULL;
	long *gamma = NULL;
	char *pattern = NULL;
	char *sub = NULL;
	long pattern_len;
	FILE *fp;

	printf("Super arma\n");
	pattern_len = SPWEAPON_LEN;
	lambda = last_occurrence(SPWEAPON_SEQ,pattern_len);
	gamma = good_suffix(SPWEAPON_SEQ,pattern_len);
	pattern = copy_pattern(SPWEAPON_SEQ,pattern_len);
	sub = copy_pattern(SPWEAPON_REP,pattern_len);
	if ((fp = fopen(arg,"r+")) != NULL) {
		replace(pattern,pattern_len,sub,fp,arg,lambda,gamma);
		fclose(fp);
	} else
		printf("Non trovo il file %s\n",arg);
	free(pattern);
	free(sub);
	free(lambda);
	free(gamma);
}

int replace(char *ptn, int ptn_len, char *sb, FILE *fp, char *fn, long *l, long *g) {
	long pos;
	int i;

	pos = unbuffered_matcher(fp,fn,ptn,ptn_len,l,g);
	if (pos != -1) {
		printf("Trovato <");
		print_pattern(ptn,ptn_len);
		printf("> alla posizione %d\n",pos);
		for (i=0; i<ptn_len; i++)
			fputc(sb[i],fp);
		printf("Sostituito pattern con <");
		print_pattern(sb,ptn_len);
		printf(">\n");
	}
	return pos;
}

int menu(void) {
	int risp;

	printf("1 - Invulnerabilita'\n");
	printf("2 - Passa parete\n");
	printf("3 - Super arma\n");
	printf("0 - Uscita\n");
	do {
		printf("> ");
		risp = get_int();
	} while ((risp != 0) && (risp != 1) && (risp != 2) && (risp != 3));
	return risp;
}

int main(int argc, char *argv[]) {
	int risp;

	if (argc != 2) {
		printf("usage: %s <file.exe>\n",argv[0]);
		exit(1);
	}
	do {
		risp = menu();
		switch (risp) {
			case 1:	invulnerability(argv[1]);
				break;
			case 2:	walk_through(argv[1]);
				break;
			case 3:	super_weapon(argv[1]);
				break;
		}
	} while (risp == 0);
	return 0;
}
*/