// eob_editor.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "eob_viewer.h"

int main()
{
	const char * exePath = "..\\..\\..\\data\\eob.exe";
	eob::eob_viewer viewer(exePath);
}