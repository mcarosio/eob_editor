#pragma once

#include <cstdint>
#include <filesystem>
#include <algorithm>

namespace utils
{
	namespace binary
	{
		class buffer
		{
		public:

			struct buffer_size
			{
				static const size_t kilo = 1024;
				static const size_t mega = kilo * kilo;
			};

			template <typename _DataType>
			class iterator
			{
			public:
				iterator(buffer* const buf, bool start) : _index(0), _buffer(buf)
				{
				}
				iterator(buffer* const buf) : _index(buf->length()), _buffer(buf)
				{
				}
				~iterator() {}
					
				operator bool() const
				{
					return _index == _buffer->length();
				}
				bool operator==(const iterator& it) const
				{
					return (_index == it._index);
				}
				bool operator!=(const iterator& it) const
				{
					return (_index != it._index);
				}
				iterator& operator=(iterator& it)
				{
					this->_index = it._index;
					this->_buffer = it._buffer;
					it._buffer = nullptr;	//takes ownership
					return (*this);
				}
				iterator& operator+=(const int& movement)
				{
					_index += movement * sizeof(_DataType);
					return (*this);
				}
				iterator& operator++()
				{
					_index += sizeof(_DataType);
					return (*this);
				}
				iterator operator++(int)
				{
					auto temp(*this);
					++(*this);
					return temp;
				}
				const _DataType& operator*() const
				{
					return (*_buffer)[_index];
				}
				size_t index() const { return _index; }
				
			protected:
				size_t						_index;
				buffer						*_buffer;
			};

			buffer(const size_t initialCapacity = 0);
			~buffer();

			void reserve(const size_t length) { m_length = length; }

			const uint8_t* const data(const size_t offset = 0) const { return m_data + offset; }
			const size_t& length() const { return m_length; }
			const size_t& capacity() const { return m_capacity; }

			void load(const std::experimental::filesystem::path& filePath);
			void load(std::ifstream& inputStream, const std::string& filePath);
			void resize(const size_t& newCapacity);
			void erase(const size_t& offset, const size_t& length);
			void save(const std::experimental::filesystem::path& filePath);
			void save(std::ofstream& outputFile, const std::string& filePath);
			void save(const std::string& filePath);
			void save(const char* filePath);
			void save(const char* filePath, const size_t pathLength);

			template <typename _InData>
			void replace(const _InData* inputBytes, const size_t& offset, const size_t replaceLength = 0)
			{
				replace(inputBytes, sizeof(_InData), offset, replaceLength);
			}

			template <typename _InData>
			void replace(const _InData* inputBytes, size_t inputSize, const size_t& offset, const size_t replaceLength = 0)
			{
				size_t trailingSize = (m_length < (offset + replaceLength)) ? 0 : m_length - (offset + replaceLength);
				size_t newSize = offset + inputSize + trailingSize;
				m_capacity = std::max<size_t>(m_capacity, newSize);
				uint8_t *newData = new uint8_t[m_capacity];
				memcpy(newData, m_data, offset);
				memcpy(newData+offset, inputBytes, inputSize);
				memcpy(newData+offset+inputSize, m_data+m_length-trailingSize, trailingSize);

				if (m_data != nullptr)
					delete[] m_data;
				m_data = newData;
				m_length = newSize;
			}

			template <typename _DataType, size_t offsetMultiplier = sizeof(_DataType)>
			_DataType* data(const size_t idx)
			{
				size_t offset = idx * offsetMultiplier;
				if (offset > m_length)
					throw std::exception("Index out of bound", idx);

				uint8_t *ptr = m_data + offset;
				_DataType *raw = reinterpret_cast<_DataType*>(ptr);
				return raw;
			}

			char const* ascii(const size_t idx)
			{
				if (idx > m_length)
					throw std::exception("Index out of bound", idx);

				return reinterpret_cast<char*>(m_data + idx);
			}

			const char& character(const size_t idx)
			{
				if (idx > m_length)
					throw std::exception("Index out of bound", idx);

				return reinterpret_cast<char>(m_data + idx);
			}

			template <typename _OutType, size_t offsetMultiplier = sizeof(_OutType)>
			static const _OutType& parse(uint8_t const *data, const size_t dataLength, const size_t offset)
			{
				if (offset * offsetMultiplier > dataLength)
					throw std::exception("Index out of bound", offset);

				const uint8_t *ptr = data + (offset * offsetMultiplier);
				const _OutType *raw = reinterpret_cast<const _OutType*>(ptr);
				return *raw;
			}

			const uint8_t& operator[] (const size_t idx);
			buffer& operator= (const buffer& input);
			buffer& operator+= (const buffer& input);
			buffer& operator= (const char* input);
			buffer& operator+= (const char* input);
			buffer& operator= (const std::vector<uint8_t>& input);
			buffer& operator+= (const std::vector<uint8_t>& input);
			
			bool operator== (const buffer& input);
			bool operator!= (const buffer& input);
			
			template <typename _InType>
			buffer& assign(const _InType* data)
			{
				return assign(data, sizeof(_InType));
			}

			template <typename _InType>
			buffer& assign(const _InType* data, const size_t dataLength)
			{
				const uint8_t *newData = reinterpret_cast<const uint8_t*>(data);
				resize(dataLength);
				memcpy(m_data, newData, dataLength);
				if (m_length == 0)
					m_length = dataLength;
				return (*this);
			}

			template <typename _InType>
			buffer& append(const _InType* data)
			{
				return append(data, sizeof(_InType));
			}

			template <typename _InType>
			buffer& append(const _InType* data, const size_t dataLength)
			{
				size_t oldSize = m_length;
				const uint8_t *newData = reinterpret_cast<const uint8_t*>(data);
				resize(oldSize +dataLength);
				memcpy(m_data+oldSize, newData, dataLength);
				return (*this);
			}

			friend std::ostream& operator<< (std::ostream& out, buffer& buf)
			{
				out.write(reinterpret_cast<const char*>(buf.data()), buf.length());
				return out;
			}
			
			std::string to_hex();

			template <typename _OutType = uint8_t>
			auto begin()
			{
				return buffer::iterator<_OutType>(this, true);
			}

			template <typename _OutType = uint8_t>
			auto end()
			{
				return buffer::iterator<_OutType>(this);
			}

		protected:
			uint8_t		*m_data;
			size_t		m_length;
			size_t		m_capacity;
		};
	}
}