#pragma once

#include "sqlite\sqlite3.h"

#include "utils.h"

#include <map>
#include <functional>
#include <filesystem>

namespace utils
{
	namespace db
	{
		class sqlite_db
		{
			public:

				using statement_id = size_t;
				using prepared_statement_set = std::map<statement_id, sqlite3_stmt*>;
				using statement_name_set = std::map<std::string, statement_id>;

				enum class source
				{
					file,
					memory
				};

				enum class status
				{
					unknown,
					bound,
					open,
					close
				};

				enum class error_handler_style
				{
					internal,
					exception
				};

				class record : public std::vector<std::string>
				{
					friend std::ostream& operator<< (std::ostream &out, const utils::db::sqlite_db::record &r)
					{
						std::stringstream ss;
						size_t c = 0;
						for (utils::db::sqlite_db::record::const_iterator it = r.cbegin(); it!=r.cend(); ++it)
						{
							ss << *it;
							if (c < r.size())
							{
								ss << ", ";
							}
							++c;
						}
						out << ss.str();
						return out;
					}
				};

				using visit_callback = std::function<void (const utils::db::sqlite_db::record&)>;

				class iterator
				{
					friend class sqlite_db;
					public:

						iterator(sqlite_db* const db, const std::string &stmt)
							: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(nullptr)
						{
							int nByte = -1;
							const char *pzTail = NULL;

							m_result = sqlite3_prepare_v2(m_db->db_connection(), stmt.c_str(), nByte, &m_stmt, &pzTail);
							if (m_result != SQLITE_OK)
								throw std::exception(sqlite3_errmsg(m_db->db_connection()));

							m_numCols = sqlite3_column_count(m_stmt);
							m_result = sqlite3_step(m_stmt);
							if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
							{
								throw std::exception(sqlite3_errmsg(m_db->db_connection()));
							}
						}
						iterator(sqlite_db* const db, sqlite3_stmt *stmt)
							: m_result(SQLITE_OK), m_db(db), m_numCols(0), m_stmt(stmt)
						{
							m_numCols = sqlite3_column_count(m_stmt);
							m_result = sqlite3_step(m_stmt);
							if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
							{
								throw std::exception(sqlite3_errmsg(m_db->db_connection()));
							}
						}
						iterator(sqlite_db* const db, const statement_id& stmtId)
							: m_result(SQLITE_OK), m_db(db), m_numCols(0)//, m_stmt(stmt)
						{
							m_stmt = db->m_preparedStatements[stmtId];
							m_numCols = sqlite3_column_count(m_stmt);
							m_result = sqlite3_step(m_stmt);
							if (m_result != SQLITE_ROW && m_result != SQLITE_DONE)
							{
								throw std::exception(sqlite3_errmsg(m_db->db_connection()));
							}
						}
				//		iterator(sqlite_db* const db)
				//			: m_result(SQLITE_DONE), m_db(db), m_numCols(0), m_stmt(nullptr)
				//		{
				//		}
						iterator()
							: m_result(SQLITE_DONE), m_db(nullptr), m_numCols(0), m_stmt(nullptr)
						{
						}
						~iterator()
						{
							if (m_stmt)
							{
								m_result = sqlite3_reset(m_stmt);
								m_stmt = nullptr;
							}
						}
					
						operator bool()const
						{
							return m_result == SQLITE_ROW;
						}
					
						bool operator==(const iterator& it) const
						{
							return (m_result == it.m_result);
						}
						bool operator!=(const iterator& it) const
						{
							return (m_result != it.m_result);
						}
						iterator& operator=(iterator& it)
						{
							this->m_result = it.m_result;
							this->m_db = it.m_db;
							this->m_numCols = it.m_numCols;
							this->m_stmt = it.m_stmt;
							it.m_stmt = nullptr;	//takes ownership
							return (*this);
						}
					
						iterator& operator+=(const int& movement)
						{
							for (int i=0; i<movement; ++i)
							{
								m_result = sqlite3_step(m_stmt);
							}
							return (*this);
						}
						iterator& operator++()
						{
							m_result = sqlite3_step(m_stmt);
							return (*this);
						}
						iterator operator++(int)
						{
							auto temp(*this);
							++(*this);
							return temp;
						}
						utils::db::sqlite_db::record operator*()
						{
							utils::db::sqlite_db::record row;
							const unsigned char *zValue = nullptr;
							for (long col=0; col<m_numCols; ++col)
							{
								zValue = sqlite3_column_text(m_stmt, col);
								if (zValue)
								{
									row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
								}
								else
								{
									row.push_back("");
								}
							}
							return row;
						}
						const utils::db::sqlite_db::record operator*() const
						{
							utils::db::sqlite_db::record row;
							const unsigned char *zValue = NULL;
							for (long col=0; col<m_numCols; ++col)
							{
								zValue = sqlite3_column_text(m_stmt, col);
								if (zValue)
								{
									row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
								}
							}
							return static_cast<const utils::db::sqlite_db::record>(row);
						}
						int get_result() const
						{
							return m_result;
						}
						sqlite3_stmt const * statement() const
						{
							return m_stmt;
						}
						const int& columns_count() const
						{
							return m_numCols;
						}

						const utils::db::sqlite_db::record get_caption() const
						{
							utils::db::sqlite_db::record row;
							const char *zValue = NULL;
							for (long col=0; col<m_numCols; ++col)
							{
								zValue = sqlite3_column_name(m_stmt, col);
								if (zValue)
								{
									row.push_back(std::string(reinterpret_cast<const char*>(zValue)));
								}
							}
							return static_cast<const utils::db::sqlite_db::record>(row);
						}

						const std::string get_caption(int column) const
						{
							const char *zCaption = NULL;
							if (column > m_numCols)
							{
								throw std::exception("Column index out of bound");
							}
							zCaption = sqlite3_column_name(m_stmt, column);
							if (!zCaption)
								throw std::exception("Error retrieving data");
							return std::string(zCaption);
						}

						template <typename _Type>
						_Type data(const int col, const _Type& defaultValue) const
						{
							const char *zValue = NULL;
							if (col < 0 || col > m_numCols)
							{
								throw std::exception("Column index out of bound");
							}
						
							zValue = reinterpret_cast<const char*>(sqlite3_column_text(m_stmt, col-1));
							if (!zValue)
								throw std::exception("Error retrieving data");

							return utils::ascii_utils::parse<_Type>(zValue, std::strlen(zValue), defaultValue);
						}

						std::string sql() const
						{
							return sqlite3_sql(m_stmt);
						}

					protected:
						int							m_result;
						sqlite_db					*m_db;
						int							m_numCols;
						sqlite3_stmt				*m_stmt;
				};

				sqlite_db(void);
				virtual ~sqlite_db(void);
				
				void error(const char* text);
				void error(const std::string& text);
				
				bool bind();
				bool open();
				bool close(bool saveData = false);
			
				bool clean_db(bool dropTables = false);
			
				bool execute(const std::experimental::filesystem::path& scriptPath);
				bool execute(const std::string& sqlScript);
				bool execute(const char* sqlScript);
				bool execute(const statement_id& stmtId);
				bool execute(const statement_id& stmtId, iterator& itBegin, iterator& itEnd);
				bool execute(const std::string &stmt, utils::db::sqlite_db::record& firstRow);
				bool execute(const statement_id& stmtId, record& firstRow);
				bool execute(const char* stmt, const visit_callback& f);
				bool execute(const std::string& stmt, const visit_callback& f);
				bool execute(const statement_id& stmtId, const visit_callback& f);

				template <typename T>
				bool execute(const std::string &stmt, T& value)
				{
					utils::db::sqlite_db::record r;
					bool ok = execute(stmt, r);
					value = utils::ascii_utils::parse<T>(r[0].c_str(), r[0].length());
					return ok;
				}

				template <typename T>
				bool execute(const statement_id& stmtId, T& value)
				{
					utils::db::sqlite_db::record r;
					bool ok = execute(stmtId, r);
					value = utils::ascii_utils::parse<T>(r[0].c_str(), r[0].length());
					return ok;
				}

				bool create_function(const std::string& functionName, long numArgs, void (*funct)(sqlite3_context*, int, sqlite3_value**));

				template <typename T>
				bool bind_parameter(const statement_id& stmtId, const int index, const T& value)
				{
					prepared_statement_set::iterator it = m_preparedStatements.find(stmtId);
					if (it == m_preparedStatements.end())
					{
						error("Invalid prepared statement id");
						return false;
					}

					sqlite3_stmt *prepStmt = m_preparedStatements[stmtId];
					sqlite3_value v = value;
					return sqlite3_bind_value(prepStmt, index, value) == SQLITE_OK;
				}
				//bool bind_parameter(const statement_id& stmtId, int index, const long& value);
				//bool bind_parameter(const statement_id& stmtId, int index, const double& value);
				bool bind_parameter(const statement_id& stmtId, int index, const std::time_t& value);
				bool bind_parameter(const statement_id& stmtId, int index, const std::string& value);
				bool bind_parameter(const statement_id& stmtId, int index, const char* value);
				void clean_prepared_statements();

				iterator begin(const std::string &stmt);
				iterator begin(sqlite3_stmt *stmt);
				iterator end();
			
				bool begin_transaction() { return execute("BEGIN TRANSACTION"); }
				bool commit_transaction() { return execute("COMMIT TRANSACTION"); }
				bool rollback_transaction() { return execute("ROLLBACK TRANSACTION"); }

				bool explain_query_plan(const std::string &sql, std::vector<std::string> &output);
				unsigned long count_rows(const std::string &tableName);
				bool save(const std::string& dbPathDest);
				bool restore(const std::string& dbSrc);

				std::string sql(const statement_id& stmtId);
				bool register_function(const std::string& functionName, long numArgs, void (*func)(sqlite3_context*, int, sqlite3_value**), std::string &error);
				
				utils::defaultable<statement_id> prepare_statement(const std::string& statement, const std::string& symbolicName = "");

				sqlite3* const db_connection() const { return m_dbConnection; };
				std::string& db_path() { return m_dbPath; }
				utils::db::sqlite_db::source& db_source() { return m_dbSource; }
				bool is_open() { return m_status == status::open; }
				std::string db_location();
				const std::string& last_error() { return m_lastError; }
				const statement_id& resolve_by_name(const std::string& statementName) { return m_preparedStatementNames[statementName]; }
				error_handler_style& error_handler() { return m_errorHandler; }

				bool add_pragma(const std::string &key, const std::string &value);

			protected:
				status							m_status;
				std::string						m_dbPath;
				utils::db::sqlite_db::source	m_dbSource;
				sqlite3							*m_dbConnection;
				prepared_statement_set			m_preparedStatements;
				statement_name_set				m_preparedStatementNames;
				error_handler_style				m_errorHandler;
				std::string						m_lastError;

				sqlite3_stmt* get_prepared_statement(const utils::db::sqlite_db::statement_id& stmtId);
		};
	}
}