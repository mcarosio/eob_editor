#pragma once

#include "binary_utils.h"

#include <filesystem>
#include <map>
#include <sstream>

namespace utils
{
	namespace zip
	{
		using zip_header = struct _zip_h
		{};

		using zip_local_file_header = struct _zip_lfh : public zip_header
		{
			char	signature[4];
			char	creatorVersion[2];
			char	extractorVersion[2];
			char	flags[2];
			char	lastModificationTime[2];
			char	lastModificationDate[2];
			char	checksum[4];
			char	compressedSize[4];
			char	uncompressedSize[4];
			char	filenameSize[2];
			char	extraFieldSize[2];
		};

		using zip_central_directory_file_header = struct _zip_cdfh : public zip_header
		{
			char	signature[4];
			char	version[2];
			char	versionNeeded[2];
			char	flags[2];
			char	compression[2];
			char	lastModificationTime[2];
			char	lastModificationDate[2];
			char	checksum[4];
			char	compressedSize[4];
			char	uncompressedSize[4];
			char	filenameSize[2];
			char	extraFieldSize[2];
			char	fileCommentSize[2];
			char	segmentFileDiskNumber[2];
			char	internalFileAttributes[2];
			char	externalFileAttributes[4];
			char	localHeaderOffset[4];
		};

		using zip_end_of_central_directory_header = struct _zip_eocdr : public zip_header
		{
			char	signature[4];
			char	diskNumber[2];
			char	diskWCD[2];
			char	diskEntries[2];
			char	totalEntries[2];
			char	centralDiectorySize[4];
			char	offsetToCDWrtStartingDisk[4];
			char	commentSize[2];
		};

		enum class zip_flag
		{
			encrypted_file,						//Bit 00: encrypted file
			compression_option,					//Bit 01: compression option
			//compression_option,				//Bit 02: compression option
			data_descriptor,					//Bit 03: data descriptor
			enhanced_deflation,					//Bit 04: enhanced deflation
			compressed_patched_data,			//Bit 05: compressed patched data
			strong_encryption,					//Bit 06: strong encryption
			//Bit 07-10: unused
			language_encoding,					//Bit 11: language encoding
			//Bit 12: reserved
			mask_header_values					//Bit 13: mask header values
			//Bit 14-15: reserved 
		};

		enum class compression_level
		{
			default_compression = -1,
			level_0_none = 0,
			level_1_best_speed = 1,
			level_2 = 2,
			level_3 = 3,
			level_4 = 4,
			level_5 = 5,
			level_6_default_level = 6,
			level_7 = 7,
			level_8 = 8,
			level_9_best_compression = 9,
			level_10_uber_compression
		};

		enum class compression_strategy
		{
			default_strategy,
			filtered,
			huffman_only,
			rle,
			fixed
		};

		enum class compression_method
		{
			no_compression = 0,
			shrunk = 1,
			reduced_with_compression_factor_1 = 2,
			reduced_with_compression_factor_2 = 3,
			reduced_with_compression_factor_3 = 4,
			reduced_with_compression_factor_4 = 5,
			imploded = 6,
			//07: reserved
			deflated = 8,
			enhanced_deflated = 9,
			pk_ware_dcl_imploded = 10,
			//11: reserved
			compressed_using_bzip2 = 12,
			//13: reserved
			lzma = 14,
			//15-17: reserved
			compressed_using_ibm_terse = 18,
			ibm_lz77_z = 19,
			ppmd_version_I_rev_1 = 98
		};

		enum class record_signature
		{
			central_directory_file_header,
			local_file_header,
			end_of_central_directory_record,
			unknown
		};

		class unzip_result
		{
		public:
			unzip_result() {}
			~unzip_result() {}

			auto begin() { return m_stats.begin(); }
			auto end() { return m_stats.end(); }
			auto count() { return m_stats.size(); }

			void add_result(const std::experimental::filesystem::path& filePath, const size_t& originalSize, const size_t& compressedSize)
			{
				double compressionFactor = (originalSize > 0) ? (originalSize-compressedSize) / static_cast<double>(originalSize) : 0.0;
				m_stats[filePath] = std::make_tuple(originalSize, compressedSize, compressionFactor);
			}

			size_t original_size(const std::experimental::filesystem::path& filePath)
			{
				return std::get<0>(m_stats[filePath]);
			}
			size_t compressed_size(const std::experimental::filesystem::path& filePath)
			{
				return std::get<1>(m_stats[filePath]);
			}
			double compression_factor(const std::experimental::filesystem::path& filePath)
			{
				return std::get<2>(m_stats[filePath]);
			}

		protected:
			std::map<std::experimental::filesystem::path, std::tuple<size_t, size_t, double>>
										m_stats;
		};

		class compress_job
		{
		public:
			compress_job() {}
			~compress_job() {}

			compress_job& operator+=(const std::experimental::filesystem::path& filePath)
			{
				m_files.push_back(filePath);
				return (*this);
			}
			compress_job& operator+=(const std::string& filePath)
			{
				std::experimental::filesystem::path p(filePath);
				m_files.push_back(p);
				return (*this);
			}
			compress_job& operator+=(const char* filePath)
			{
				std::experimental::filesystem::path p(filePath);
				m_files.push_back(p);
				return (*this);
			}

			auto begin() const { return m_files.cbegin(); }
			auto end() const { return m_files.cend(); }
			size_t size() const { return m_files.size(); }

		protected:
			std::list<std::experimental::filesystem::path>
										m_files;
		};

		class zip_manager
		{
		public:

			class iterator
			{
			public:
				iterator(const uint8_t* const buf, const size_t length) : _index(0), _buffer(buf), _length(length)
				{
				}
				iterator(const size_t length) : _index(length), _buffer(nullptr), _length(length)
				{
				}
				~iterator() {}
					
				operator bool() const
				{
					return _index >= 0;
				}
				bool operator==(const iterator& it) const
				{
					return (_index == it._index);
				}
				bool operator!=(const iterator& it) const
				{
					return (_index != it._index);
				}
				iterator& operator=(iterator& it)
				{
					this->_index = it._index;
					this->_buffer = it._buffer;
					it._buffer = nullptr;	//takes ownership
					return (*this);
				}
				iterator& operator+=(const int& movement)
				{
					for (int m=0; m<movement; ++m)
						++(*this);
					return (*this);
				}
				iterator& operator++()
				{
					/*if (_index == _length)
					{
						_index = -1;
						return (*this);
					}*/

					auto rs = signature();
					if (rs == record_signature::unknown)
					{
						std::stringstream ss;
						ss << "Unknown record signature at position" << _index;
						throw std::exception(ss.str().c_str());
					}
					
					_index += length(rs);

					return (*this);
				}
				iterator operator++(int)
				{
					auto temp(*this);
					++(*this);
					return temp;
				}
				const record_signature signature() const
				{
					return zip_manager::parse_signature(_buffer + _index);
				}
				template <typename _HeaderType>
				_HeaderType* header() const
				{
					//zip_header *z = &utils::binary::buffer::parse<zip_header>(_buffer, _length,  _index);
					return reinterpret_cast<_HeaderType*>((uint8_t*)_buffer + _index);
					//return dynamic_cast<_HeaderType*>(z);
				}
				size_t index() const { return _index; }

				size_t length(const record_signature& rs)
				{
					if (rs == record_signature::central_directory_file_header)
					{
						auto *h = header<zip_central_directory_file_header>();
			
						return sizeof(zip_central_directory_file_header)
								+ zip_manager::filename_size(h) + zip_manager::extra_field_size(h) + zip_manager::file_comment_size(h);
					}
					else if (rs == record_signature::end_of_central_directory_record)
					{
						auto *h = header<zip_end_of_central_directory_header>();

						return sizeof(zip_end_of_central_directory_header) + zip_manager::file_comment_size(h);
					}
					else if (rs == record_signature::local_file_header)
					{
						auto *h = header<zip_local_file_header>();
						return sizeof(zip_local_file_header)
								+ zip_manager::compressed_size(h) + utils::zip::zip_manager::filename_size(h)
								+ utils::zip::zip_manager::extra_field_size(h);
					}
				}
				
			protected:
				int				_index;
				const uint8_t	*_buffer;
				size_t			_length;
			};

			zip_manager() {}
			/*zip_manager(uint8_t *workingBuffer, const size_t workingBufferLength)
				: m_workingBuffer(workingBuffer), m_workingBufferLength(workingBufferLength)
			{
			}*/
			~zip_manager() {}
			
			static unzip_result decompress_archive(const std::experimental::filesystem::path& inFilePath, const std::experimental::filesystem::path& outFilePath, bool createIfNotExists = true);
			static unzip_result decompress_archive(const std::string& inFilePath, const std::string& outFilePath, bool createIfNotExists = true);
			static unzip_result decompress_archive(const char* inFilePath, const char* outFilePath, bool createIfNotExists = true);
			static unzip_result decompress_archive(const uint8_t* zippedBuffer, const size_t& zippedLength, const char* outFilePath, bool createIfNotExists);

			static void compress_archive(const std::experimental::filesystem::path& inFilePath, const std::experimental::filesystem::path& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			static void compress_archive(const std::string& inFilePath, const std::string& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			static void compress_archive(const char* inFilePath, const char* outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			static void compress_archive(const compress_job& files, const std::experimental::filesystem::path& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			static void compress_archive(const compress_job& files, const std::string& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			static void compress_archive(const compress_job& files, const char* outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			
			static void decompress_buffer(const uint8_t* zippedBuffer, const size_t& zippedLength, utils::binary::buffer& outputBuffer);

			static void compress_buffer(const compress_job& files, utils::binary::buffer& outputBuffer, const compression_level& level = compression_level::level_6_default_level);
			static void compress_buffer(const uint8_t* unzippedBuffer, const size_t& unzippedLength, utils::binary::buffer& outputBuffer, const compression_level& level = compression_level::level_6_default_level);

			//static void add_to_archive(const std::experimental::filesystem::path& inFilePath, const std::experimental::filesystem::path& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			//static void add_to_archive(const std::string& inFilePath, const std::string& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			//static void add_to_archive(const char* inFilePath, const char* outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			//static void add_to_archive(const compress_job& files, const std::experimental::filesystem::path& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			//static void add_to_archive(const compress_job& files, const std::string& outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			//static void add_to_archive(const compress_job& files, const char* outFilePath, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);
			////static void add_to_archive(const uint8_t* unzippedBuffer, const size_t& unzippedLength, utils::binary::buffer& outputBuffer, const compression_level& level = compression_level::level_6_default_level, const compression_strategy& strategy = compression_strategy::default_strategy);


			static const char* signature(const zip_local_file_header* header)
			{
				return parse_header<const char*>(reinterpret_cast<const uint8_t*>(header->signature));
			}
			static const uint16_t creator_version(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->creatorVersion));
			}
			static const uint16_t extractor_version(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->extractorVersion));
			}
			static const uint16_t flags(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->flags));
			}
			static const uint16_t last_modification_time(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->lastModificationTime));
			}
			static const uint16_t last_modification_date(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->lastModificationDate));
			}
			static const uint32_t checksum(const zip_local_file_header* header)
			{
				return *parse_header<const uint32_t*>(reinterpret_cast<const uint8_t*>(header->checksum));
			}
			static const uint32_t uncompressed_size(const zip_local_file_header* header)
			{
				return *parse_header<const uint32_t*>(reinterpret_cast<const uint8_t*>(header->uncompressedSize));
			}
			static const uint32_t compressed_size(const zip_local_file_header* header)
			{
				return *parse_header<const uint32_t*>(reinterpret_cast<const uint8_t*>(header->compressedSize));
			}
			static const uint16_t filename_size(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->filenameSize));
			}
			static const uint16_t extra_field_size(const zip_local_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->extraFieldSize));
			}

			
			static const uint16_t filename_size(const zip_central_directory_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->filenameSize));
			}
			static const uint16_t extra_field_size(const zip_central_directory_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->extraFieldSize));
			}
			static const uint16_t file_comment_size(const zip_central_directory_file_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->fileCommentSize));
			}
			
			static const uint16_t file_comment_size(const zip_end_of_central_directory_header* header)
			{
				return *parse_header<const uint16_t*>(reinterpret_cast<const uint8_t*>(header->commentSize));
			}

		protected:
			/*uint8_t			*m_workingBuffer;
			size_t			m_workingBufferLength;*/

			static const size_t m_unbufferedDecompressionLimit = 768*1024*1024;

			static const uint8_t m_recordTypes[3][4];

			static record_signature parse_signature(uint8_t const * data);

			template <typename _OutType>
			static _OutType parse_header(const uint8_t* field)
			{
				return reinterpret_cast<_OutType>(field);
			}

			static auto begin(const uint8_t *workingBuffer, const size_t workingBufferLength) { return zip_manager::iterator(workingBuffer, workingBufferLength); }
			static auto end(const size_t workingBufferLength) { return zip_manager::iterator(workingBufferLength); }
		};

		enum class decompression_type
		{
			unbuffered,
			streaming
		};
	}
}