#pragma once

#include <string>
#include <map>
#include "xml_distiller.h"

namespace utils
{
	namespace config
	{
		class settings
		{
		public:
			enum class source
			{
				xml,
				cmd
			};
		
			using data_changed = std::function<void (const std::string& paramName, const std::string& oldValue, const std::string& newValue)>;

			settings(const source& src);
			virtual ~settings(void);

			virtual bool rebind();
			virtual void set_data_change_callback(const data_changed& cbk)
			{
				m_chgCbk = cbk;
			}
		
			virtual bool open(const std::string& resource) = 0;
			virtual bool open(const std::experimental::filesystem::path& resource) = 0;
			virtual bool close(bool saveData=false) = 0;
			virtual bool save() = 0;
			virtual bool get_parameter(const std::string& paramName, std::string& paramValue, bool caseSensitive=false) = 0;
			virtual bool put_parameter(const std::string& paramName, const std::string& paramValue) = 0;
		
			template <typename T>
			bool get(const std::string& paramName, T& paramValue, bool caseSensitive=false)
			{
				std::string value;
				if (!get_parameter(paramName, value, caseSensitive))
					return false;
				paramValue = utils::ascii_utils::parse<T>(value.c_str(), value.length(), paramValue);
				return true;
			}
			template <typename T>
			bool put(const std::string& paramName, const T& paramValue)
			{
				std::stringstream ss;
				ss << paramValue;
				return put_parameter(paramName, ss.str());
			}
			template <>
			bool put(const std::string& paramName, const std::string& paramValue)
			{
				return put_parameter(paramName, paramValue);
			}

		protected:
			source			m_paramSrc;
			std::string		m_resource;
			data_changed	m_chgCbk;
		};

		class xml_parameters : public settings
		{
		public:
			xml_parameters(void);
			virtual ~xml_parameters(void);

			bool open(const std::string& resource);
			bool open(const std::experimental::filesystem::path& resource);
			bool close(bool saveData=false);
			bool save();
			bool get_parameter(const std::string& paramName, std::string& paramValue, bool caseSensitive=false);
			
			/*template <bool firstOnly = true>
			bool put_parameter(const std::string& paramName, const std::string& paramValue)
			{
				auto listNodes = m_xmlDoc.query(paramName);
				if (listNodes.size() == 0)
					return false;

				if (firstOnly)
				{
					utils::xml_distiller::set_node_value(listNodes.front(), paramValue);
				}
				else
				{
					for (auto l : listNodes)
					{
						utils::xml_distiller::set_node_value(l, paramValue);
					}
				}
				return true;
			}*/

			bool put_parameter(const std::string& paramName, const std::string& paramValue);

		protected:
			xml_distiller		m_xmlDoc;
		};

		/*class cmd_parameters : public settings
		{
		public:
			cmd_parameters(void);
			virtual ~cmd_parameters(void);
		
			bool open(const std::string& resource);
			bool close(bool saveData=false);
			bool save();
			bool get_parameter(const std::string& paramName, std::string& paramValue, bool caseSensitive=false);
			bool put_parameter(const std::string& paramName, const std::string& paramValue);
	
		protected:
			std::map<std::string, std::string> m_switch;
		};*/
	};
};