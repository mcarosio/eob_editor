#pragma once

#include <string>
#include <functional>
#include <filesystem>

namespace utils
{
	namespace fs
	{
		struct dir_stat
		{
			dir_stat() : size(0), items(0), subfolders(0) {};
			 
			void reset()
			{
				size = 0;
				items = 0;
				subfolders = 0;
			}

			std::uint64_t size;
			std::uint64_t items;
			std::uint64_t subfolders;
		};

		class dir_walker
		{
			public:

				class iterator : public std::experimental::filesystem::recursive_directory_iterator
				{
					public:
						iterator(const std::experimental::filesystem::path &p)
							: std::experimental::filesystem::recursive_directory_iterator(p)
						{
							root = p;
						}
						iterator()
							: std::experimental::filesystem::recursive_directory_iterator()
						{
						}
					protected:
						std::experimental::filesystem::path root;
				};

				//enum class visit_order { depth_first = 0, breath_first };
				using visit_funct = std::function<void (const std::experimental::filesystem::path&)>;
			
				dir_walker();
				dir_walker(const std::string &path);
				dir_walker(const std::experimental::filesystem::path &p);
				virtual ~dir_walker(void);

				void init(const std::string &path);

				std::experimental::filesystem::path get_root() { return m_root; };
				//void visit(const visit_order& order, const VisitFunct& cbk);
				void visit(const visit_funct& cbk);
				void visit(const visit_funct& cbk, dir_stat &stat);

				fs::dir_walker::iterator begin()
				{
					return dir_walker::iterator(m_root);
				};
				fs::dir_walker::iterator end() { return dir_walker::iterator(); };

				//void set_filter(const std::string &filter, bool resetAfterVisit = false);

			protected:
				std::experimental::filesystem::path		m_root;
				std::string								m_filter;
				bool									m_resetFilter;
				std::vector<std::string>				m_filters;

				//bool check_filter(const std::experimental::filesystem::path &p, const std::string &f);
				//bool check_filters(const std::experimental::filesystem::path &p);
		};
	}
}