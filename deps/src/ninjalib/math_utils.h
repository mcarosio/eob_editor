#pragma once

#include <exception>
#include <functional>
#include <sstream>
#include <map>
#include <set>
#include <vector>

namespace math
{
	using factor_set = std::map<long long, int>;

	class prime_factors
	{
	public:
		prime_factors(long long n, bool compute = false) : m_num(n)
		{
			m_divs.clear();
			if (compute)
				this->compute();
		};
		virtual ~prime_factors() {};

		void compute();
		const int& operator[] (const int& idx) const
		{
			if (m_divs.find(idx) != m_divs.end())
				return m_divs.at(idx);
			else
				return std::forward<int>(0);
		}
		prime_factors& operator= (const prime_factors& divs)
		{
			this->m_divs = divs.m_divs;
			this->m_num = divs.m_num;
			return (*this);
		}
		prime_factors& operator= (const long long& num)
		{
			this->m_num = num;
			this->compute();
			return (*this);
		}
		size_t size()
		{
			return m_divs.size();
		}
		factor_set::iterator begin();
		factor_set::iterator end();
		void foreach(const std::function<void(const long long&, const int&)>& f) const;
		long long prod() const;

	protected:
		factor_set			m_divs;
		long long			m_num;
	};

	class prime_sequence : public std::set<long long>
	{
	public:
		prime_sequence(long long from, long long to) : m_from(from), m_to(to) {}
		virtual ~prime_sequence() {}

		void fill();

	protected:
		long long m_from;
		long long m_to;
	};

	class math_utils
	{
	public:
		math_utils() {};
		virtual ~math_utils() {};

		static bool is_prime(long long number);
		static long long MCD(const long long& n1, const long long& n2);
		static long long mcm(const long long& n1, const long long& n2);

	protected:
		static long long MCD(const prime_factors& d1, const prime_factors& d2);
		static long long mcm(const prime_factors& d1, const prime_factors& d2);
	};

	enum class vector_direction
	{
		right=0,
		left,
		bottom,
		top,
		top_right,
		bottom_right,
		bottom_left,
		top_left
	};

	struct cartesian_coord
	{
		size_t x_pos;
		size_t y_pos;

		cartesian_coord() : x_pos(0), y_pos(0) {}
		cartesian_coord(const size_t &x, const size_t & y) : x_pos(x), y_pos(y) {}
		cartesian_coord(const struct cartesian_coord& c) : x_pos(c.x_pos), y_pos(c.y_pos) {}

		bool operator==(const struct cartesian_coord& c) const
		{
			return (x_pos == c.x_pos && y_pos == c.y_pos);
		}
		struct cartesian_coord& operator++()
		{
			++x_pos;
			++y_pos;
			return *this;
		}
		struct cartesian_coord operator++(int)
		{
			struct cartesian_coord temp = *this;
			++x_pos;
			++y_pos;
			return temp;
		}
		struct cartesian_coord& operator--()
		{
			--x_pos;
			--y_pos;
			return *this;
		}
		struct cartesian_coord operator--(int)
		{
			struct cartesian_coord temp = *this;
			--x_pos;
			--y_pos;
			return temp;
		}

		void move_x(size_t offset) { x_pos += offset; }
		void move_y(size_t offset) { y_pos += offset; }

		static const struct cartesian_coord origin;
	};
	
	template<typename T = double>
	class matrix
	{
	public:
	
		using init_funct = std::function<T (const size_t& h, const size_t& w)>;
		using visit_funct = std::function<void (const size_t& idx)>;

		class linear_subspace
		{
			public:

				linear_subspace(matrix<T> * const m, const cartesian_coord& orig /*= cartesian_coord::ORIGIN*/, const vector_direction &dir = vector_direction::bottom)
					: m_matrix(m), m_origin(orig), m_dir(dir)
				{}
				~linear_subspace(){}

				bool operator==(const linear_subspace& ls) const
				{
					return (m_origin == ls.m_origin);
				}
				bool operator!=(const linear_subspace& ls) const
				{
					return !(m_origin == ls.m_origin);
				}
				linear_subspace& operator++()
				{
					if (m_dir == vector_direction::right || m_dir == vector_direction::left)
					{
						m_origin.y_pos = (m_origin.y_pos + 1) % m_matrix->count_rows();
					}
					else if (m_dir == vector_direction::bottom || m_dir == vector_direction::top)
					{
						m_origin.x_pos = (m_origin.x_pos + 1) % m_matrix->count_columns();
					}
					else
					{
						throw std::exception("Invalid direction, unable to increment");
					}
					return (*this);
				}

				linear_subspace& operator= (const linear_subspace &sp)
				{
					m_matrix = sp.m_matrix;
					m_origin = sp.m_origin;
					m_dir = sp.m_dir;
					return (*this);
				}

				T& operator[] (const size_t pos)
				{
					if (!m_matrix)
					{
						throw std::exception("Linear_subspace not initialised");
					}

					size_t r = 0;
					size_t c = 0;
					if (m_dir == vector_direction::right)
					{
						r = (m_origin.y_pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom)
					{
						r = (m_origin.y_pos + pos) % m_matrix->count_rows();
						c = (m_origin.x_pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::top_right)
					{
						r = (m_origin.y_pos - pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom_right)
					{
						r = (m_origin.y_pos + pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom_left)
					{
						r = (m_origin.y_pos - pos) % m_matrix->count_rows();
						c = (m_origin.x_pos - pos) % m_matrix->count_columns();
					}

					return (*m_matrix)(static_cast<int>(r), static_cast<int>(c));
				}
				/*const T& operator[] (const size_t pos) const
				{
					if (!m_matrix)
					{
						throw std::exception("Linear_subspace not initialised");
					}
					
					size_t r = 0;
					size_t c = 0;
					if (m_dir == vector_direction::right)
					{
						r = (m_origin.y_pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom)
					{
						r = (m_origin.y_pos + pos) % m_matrix->count_rows();
						c = (m_origin.x_pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::top_right)
					{
						r = (m_origin.y_pos - pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom_right)
					{
						r = (m_origin.y_pos + pos) % m_matrix->count_rows();
						c = (m_origin.x_pos + pos) % m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom_left)
					{
						r = (m_origin.y_pos - pos) % m_matrix->count_rows();
						c = (m_origin.x_pos - pos) % m_matrix->count_columns();
					}

					return (*m_matrix)(r,c);
				}*/

				size_t get_length() const
				{
					size_t len = 0;
					if (m_dir == vector_direction::right)
					{
						len = m_matrix->count_columns();
					}
					else if (m_dir == vector_direction::bottom)
					{
						len = m_matrix->count_rows();
					}
					else
					{
						len = __min(m_matrix->count_columns(), m_matrix->count_rows());
					}

					return len;
				}

				T sum()
				{
					T t();
					for (size_t i=0; i<get_length(); ++i)
						t = (*this)[i];
					return t;
				}

				void foreach(const visit_funct& f, size_t start = 0, size_t end = -1)
				{
					size_t len = (end == -1) ? get_length() : end;
					for (size_t i=start; i<len; ++i)
						f(i);
				}

				const vector_direction& get_direction() { return m_dir; }
				cartesian_coord& get_origin() { return m_origin; }

			protected:
				matrix<T>			*m_matrix;
				cartesian_coord		m_origin;
				vector_direction	m_dir;
		};

		matrix(size_t height = 1, size_t width = 1)
			: m_rows(height), m_columns(width), m_data(nullptr), m_subspace(this, cartesian_coord(0,0), vector_direction::bottom)
		{
			init();
		}
		matrix(const init_funct& init_func, size_t height = 1, size_t width = 1)
			: m_rows(height), m_columns(width), m_data(nullptr), m_subspace(this, cartesian_coord(0,0), vector_direction::bottom)
		{
			init(init_func);
		}
		matrix(const matrix<T>& m)
			: m_rows(m.m_rows), m_columns(m.m_columns), m_data(nullptr), m_subspace(this, cartesian_coord(0,0), vector_direction::bottom)
		{
			init_funct f = [&m] (const size_t& h, const size_t& w) -> T { return m.m_data[h][w]; };
			init(f);
		}

		virtual ~matrix(void)
		{
			dispose();
		};

		void dispose(void)
		{
			m_rows = 0;
			m_columns = 0;
			for (size_t r=0; r<m_rows; ++r)
				delete[] m_data[r];

			delete m_data;
			m_data = nullptr;
		};

		const size_t& count_rows() const { return m_rows; }
		const size_t& count_columns() const { return m_columns; }
		
		void init(const init_funct& init_func = nullptr)
		{
			if (!m_data)
			{
				m_data = new T*[m_rows];
				for (size_t r=0; r<m_rows; ++r)
				{
					m_data[r] = new T[m_columns];
				}
			}
			
			T defaultVal = T();
			for (size_t r=0; r<m_rows; ++r)
			{
				for (size_t c=0; c<m_columns; ++c)
				{
					if (init_func)
					{
						m_data[r][c] = init_func(r, c);
					}
					else
					{
						m_data[r][c] = defaultVal;
					}
				}
			}
			//init_funct f = [&] (const size_t &r, const size_t &c) -> T { return (init_funct) ? init_func(r, c) : defaultVal; };
			//foreach(f);
		}
		
		linear_subspace& subspace(const cartesian_coord& orig, const vector_direction& dir)
		{
			m_subspace = linear_subspace(this, orig, dir);
			return m_subspace;
		}
		
		linear_subspace& subspace(const size_t& x, const size_t& y, const vector_direction& dir)
		{
			m_subspace = linear_subspace(this, cartesian_coord(x, y), dir);
			return m_subspace;
		}

		template<typename _RetType = T>
		static _RetType null_value()
		{
			return static_cast<_RetType>(T());
		}
		
		template<typename _RetType = T>
		_RetType det()
		{
			if (m_rows != m_columns)
			{
				throw std::exception("Invalid matrix order, must be squared");
			}
			if (m_rows < 2)
			{
				throw std::exception("Invalid matrix order, must be at least 2");
			}

			if (m_rows == 2)
			{
				return det_base<_RetType>();
			}
			else if (m_rows == 3)
			{
				return sarrus<_RetType>();
			}
			else
			{
				return laplace<_RetType>(*this);
			}

			return _RetType();
		}

		static bool is_triangular_upper(matrix<T>& m)
		{
			bool triangular = true;
			
			size_t siz = __min(m.count_columns(), m.count_rows());
			for (size_t r=1, c=0; c < siz && triangular; ++r, ++c)
			{
				linear_subspace sp = m.subspace(cartesian_coord(c, r), vector_direction::bottom);

				visit_funct f_zero = [&] (const size_t &idx) -> void { if (sp[idx] != null_value()) triangular = false; };
			
				sp.foreach(f_zero, 0, siz-r);
			}
			return triangular;
		}

		static bool is_triangular_lower(matrix<T>& m)
		{
			bool triangular = true;
			
			size_t siz = __min(m.count_columns(), m.count_rows());
			for (size_t r=0, c=1; c < siz && triangular; ++r, ++c)
			{
				linear_subspace sp = m.subspace(cartesian_coord(c, r), vector_direction::right);
				
				visit_funct f_zero = [&] (const size_t &idx) -> void { if (sp[idx] != null_value()) triangular = false; };
				
				sp.foreach(f_zero, 0, siz-c);
			}
			return triangular;
		}

		static bool is_invertible(matrix<T>& m)
		{
			return m.det() != null_value();
		}

		static matrix<T> transpose(matrix<T>& m)
		{
			matrix<T> t(m.count_columns(), m.count_rows());
			init_funct f = [&] (const size_t& r, const size_t& c) -> T { return m(static_cast<int>(c), static_cast<int>(r)); };
			t.init(f);
			return t;
		}

		static matrix<T> algebraic_complement(const matrix<T>& m)
		{
			matrix<T> algCompl(m.count_rows(), m.count_columns());
			matrix<T> temp(m.count_rows()-1, m.count_columns()-1);
			
			for (size_t row=0; row<m.count_rows(); ++row)
			{
				for (size_t col=0; col<m.count_columns(); ++col)
				{
					init_funct f = [&] (const size_t& r, const size_t& c) -> T { return m.m_data[(r < row) ? r : r+1][(c < col) ? c: c+1]; };
					temp.init(f);

					T d = temp.det<T>();
					T p = std::pow(-1.0, static_cast<double>(row+col));
					algCompl(static_cast<int>(row), static_cast<int>(col)) = p * d;
				}
			}

			return algCompl;
		}

		static matrix<T> inverse(matrix<T>& m)
		{
			matrix<T> inv(m.count_columns(), m.count_rows());
			T d = m.det();

			if (d == 0)
				throw std::exception("Inverse matrix does not exists");

			matrix<T> algCompl = algebraic_complement(m);

			std::cout << algCompl << "\n";

			matrix<T> tr = transpose(algCompl);
			std::cout << tr << "\n";

			init_funct f = [&] (const size_t& r, const size_t& c) -> T { return tr.m_data[r][c] / d; };
			inv.init(f);
			return inv;
		}

		bool gauss_elimination(matrix<T>& res)
		{
			if (this == &res)
				false;

			res.dispose();
			res = *this;

			cartesian_coord pivot(0,0);
			while (pivot.x_pos < __min(res.count_columns(),res.count_rows()))
			{
				if (res(pivot) == null_value())
				{
					bool swap = false;
					size_t r = pivot.y_pos+1;
					while (r<res.count_rows() && !swap)
					{
						if (res(static_cast<int>(r), static_cast<int>(pivot.x_pos)) != null_value()) swap = true;
						else ++r;
					}
					if (swap)
					{
						linear_subspace sp1 = res.subspace(cartesian_coord(0,pivot.y_pos), vector_direction::right);
						linear_subspace sp2 = res.subspace(cartesian_coord(0,r), vector_direction::right);
						res.swap(sp1, sp2, res.count_columns());
					}
				}

				for (size_t row=pivot.y_pos+1; row<res.count_rows(); ++row)
				{
					linear_subspace sp = res.subspace(0, row, vector_direction::right);
					if (sp[pivot.x_pos] != null_value())
					{
						T k = sp[pivot.x_pos] / res(static_cast<int>(pivot.y_pos), static_cast<int>(pivot.x_pos));
						for (size_t i=pivot.x_pos; i<res.count_columns(); ++i)
						{
							sp[i] = sp[i] - k*res(static_cast<int>(pivot.y_pos), static_cast<int>(i));
						}
					}
				}
				++pivot;
			}

			return true;
		}

		/*bool gauss_jordan_elimination(matrix<T>& res)
		{
			if (!gauss_elimination(res))
				return false;

			std::cout << res << "\n";

			size_t N = __min(res.count_columns(),res.count_rows());
			cartesian_coord pivot(N-1,N-1);

			for (size_t s=0; s<N-1; ++s)
			{
				if (res(pivot) == null_value())
				{
					bool swap = false;
					size_t r = 1;
					while (r<res.count_rows() && !swap)
					{
						if (res(N-r-1,N-1) != null_value()) swap = true;
						else ++r;
					}
					if (swap)
					{
						linear_subspace sp1 = res.subspace(cartesian_coord(0,pivot.y_pos), vector_direction::HORIZONTAL_E);
						linear_subspace sp2 = res.subspace(cartesian_coord(0,N-1-r), vector_direction::HORIZONTAL_E);
						res.swap(sp1, sp2, res.count_columns());
					}
				}

				std::cout << "pivot(" << pivot.y_pos << "," << pivot.x_pos << "):\n" << res << "\n";
				for (size_t row=N-pivot.y_pos; row<N; ++row)
				{
					linear_subspace sp = res.subspace(0, N-1-row, vector_direction::HORIZONTAL_E);
					if (sp[pivot.x_pos] != null_value())
					{
						T n = sp[pivot.x_pos];
						T d = res(pivot.y_pos, pivot.x_pos);
						T k = n / d;
						std::cout << "K = " << n << "/" << d << " = " << k << "\n";
						for (size_t i=pivot.x_pos-1; i<res.count_columns(); ++i)
						{
							std::cout << "sp(" << sp.get_origin().y_pos << "," << sp.get_origin().x_pos << ")[" << N-1-i << "] = " << sp[N-1-i] << " - " << k << " * " << res(pivot.y_pos, i) << " = " << sp[i] - k*res(pivot.y_pos, i) << "\n";
							sp[i] = sp[i] - k*res(pivot.y_pos, i);
						}
					}
				}
				--pivot;
			}

			return true;
		}*/
		
		bool gauss_jordan_elimination(matrix<T>& res)
		{
			if (!gauss_elimination(res))
				return false;

			std::cout << res << "\n";

			size_t N = __min(res.count_columns(),res.count_rows());
			cartesian_coord pivot(N-1,N-1);

			//while (pivot.x_pos < __min(res.count_columns(),res.count_rows()))
			for (size_t s=0; s<N-1; s++)
			{
				if (res(pivot) == null_value())
				{
					bool swap = false;
					size_t r = pivot.y_pos-1;
					//while (r<N-1 && !swap)
					for (size_t sr=0; sr<pivot.y_pos; ++sr)
					{
						if (res(static_cast<int>(r), static_cast<int>(pivot.x_pos)) != null_value()) swap = true;
						else --r;
					}
					if (swap)
					{
						linear_subspace sp1 = res.subspace(cartesian_coord(0,pivot.y_pos), vector_direction::right);
						linear_subspace sp2 = res.subspace(cartesian_coord(0,r), vector_direction::right);
						res.swap(sp1, sp2, res.count_columns());
					}
				}
				
				std::cout << "pivot(" << pivot.y_pos << "," << pivot.x_pos << "):\n" << res << "\n";
				for (size_t row=0; row<pivot.y_pos; ++row)
				{
					linear_subspace sp = res.subspace(0, pivot.y_pos-row-1, vector_direction::right);
					if (sp[static_cast<int>(pivot.x_pos)] != null_value())
					{
						T n = sp[static_cast<int>(pivot.x_pos)];
						T d = res(static_cast<int>(pivot.y_pos), static_cast<int>(pivot.x_pos));
						T k = n / d;
						std::cout << "K = " << n << "/" << d << " = " << k << "\n";
						//for (size_t i=pivot.x_pos; i<res.count_columns(); ++i)
						for (size_t i=0; i<res.count_columns(); ++i)
						{
							std::cout << "sp(" << sp.get_origin().y_pos << "," << sp.get_origin().x_pos << ")[" << i << "] = " << sp[static_cast<const int>(i)] << " - " << k << " * " << res(static_cast<int>(pivot.y_pos), static_cast<int>(i)) << " = " << sp[static_cast<const int>(i)] - k*res(static_cast<int>(pivot.y_pos), static_cast<int>(i)) << "\n";
							sp[i] = sp[i] - k*res(static_cast<int>(pivot.y_pos), static_cast<int>(i));
						}
					}
				}
				--pivot;
			}

			return true;
		}

		size_t rank()
		{
			matrix<T> g = *this;
			this->gauss_elimination(g);
			linear_subspace sp = g.subspace(0, 0, vector_direction::bottom_right);
			size_t nonZero = 0;
			visit_funct count_non_zero = [&] (const size_t& idx) -> void { if (sp[static_cast<const int>(idx)] != 0) ++nonZero; };
			sp.foreach(count_non_zero);
			return nonZero;
		}

		static matrix<T> identity(size_t rows, size_t cols)
		{
			if (rows != cols)
				throw std::exception("Matrix order mismatch");

			matrix<T> i(rows, cols);
			init_funct f = [] (const size_t& r, const size_t& c) -> T { return (r == c) ? 1 : null_value<T>(); };
			i.init(f);
			return i;
		}

		T& operator()(int row, int col)
		{
			if (row < 0 || row >= static_cast<int>(m_rows)) throw std::exception("Row out of bound", row);
			if (col < 0 || col >= static_cast<int>(m_columns)) throw std::exception("Column out of bound", col);
 
			return m_data[row][col];
		}
		const T& operator()(int row, int col) const
		{
			if (row < 0 || row >= static_cast<int>(m_rows)) throw std::exception("Row out of bound", row);
			if (col < 0 || col >= static_cast<int>(m_columns)) throw std::exception("Column out of bound", col);
 
			return m_data[row][col];
		}
		T& operator()(const cartesian_coord &coord)
		{
			return (*this)(static_cast<int>(coord.y_pos), static_cast<int>(coord.x_pos));
		}
		const T& operator()(const cartesian_coord &coord) const
		{
			return (*this)(coord.y_pos, coord.x_pos);
		}
		
		matrix<T> operator+(const matrix<T> &rhs)
		{
			if (this->count_columns() != rhs.count_columns() || this->count_rows() != rhs.count_rows())
			{
				throw std::exception("Matrix orders mismatch");
			}
			
			//for (size_t r=0; r<this->count_rows(); ++r)
			//{
			//	for (size_t c=0; c<this->count_columns(); ++c)
			//	{
			//		this->m_data[r][c] += rhs(r,c);
			//	}
			//}
			init_funct f = [&] (const size_t &r, const size_t &c) -> T { return rhs(static_cast<int>(r), static_cast<int>(c)); };
			foreach(f);
			return *this;
		}

		matrix<T> operator*(const matrix<T> &rhs)
		{
			if (this->count_columns() != rhs.count_rows())
			{
				throw std::exception("Matrix orders mismatch");
			}

			matrix<T> res(this->count_rows(), rhs.count_columns());
			for (int r=0; r< static_cast<int>(res.count_rows()); ++r)
			{
				for (int c=0; c< static_cast<int>(res.count_columns()); ++c)
				{
					res.m_data[r][c] = this->m_data[r][0] * rhs(0,c);
					for (int i=1; i< static_cast<int>(this->count_columns()); ++i)
					{
						res.m_data[r][c] += this->m_data[r][i] * rhs(i,c);
					}
				}
			}
			return res;
		}

		matrix<T> operator*(const T &scalar)
		{
			matrix<T> res(this->count_rows(), this->count_columns());
			//for (size_t r=0; r<res.count_rows(); ++r)
			//{
			//	for (size_t c=0; c<res.count_columns(); ++c)
			//	{
			//		res.m_data[r][c] = this->m_data[r][c] * scalar;
			//	}
			//}
			
			init_funct f = [&] (const size_t &r, const size_t &c) -> T { return this->m_data[r][c] * scalar; };
			res.foreach(f);
			return res;
		}
		
		matrix<T>& operator= (const matrix<T> &m)
		{
			if (this == &m)
				return *this;
 
			if (m_data || m_rows != m.m_rows || m_columns != m.m_columns)
			{
				dispose();
				m_rows = m.m_rows;
				m_columns = m.m_columns;
				init_funct f = [&m] (const size_t& r, const size_t& c) -> T { return m.m_data[r][c]; };
				init(f);
			}
			//for (size_t r=0; r<m_rows; ++r)
			//{
			//	for (size_t c=0; c<m_columns; ++c)
			//	{
			//		m_data[r][c] = m.m_data[r][c];
			//	}
			//}
 
			return *this;
		}

	protected:
		
		using increment_funct = std::function<void (size_t& r, size_t& c)>;

		T**									m_data;
		size_t								m_columns;
		size_t								m_rows;
		linear_subspace						m_subspace;

		template<typename _RetType = T>
		_RetType det_base()
		{
			return static_cast<_RetType>(m_data[0][0]*m_data[1][1] - m_data[0][1]*m_data[1][0]);
		}

		template<typename _RetType = T>
		_RetType sarrus()
		{
			//linear_subspace sp = subspace(cartesian_coord(0, c), vector_direction::VERTICAL_S);
			return static_cast<_RetType>(m_data[0][0] * m_data[1][1] * m_data[2][2]
					+ m_data[0][1] * m_data[1][2] * m_data[2][0]
					+ m_data[0][2] * m_data[1][0] * m_data[2][1]
					- (m_data[0][2] * m_data[1][1] * m_data[2][0])
					- (m_data[0][1] * m_data[1][0] * m_data[2][2])
					- (m_data[0][0] * m_data[1][2] * m_data[2][1]));
		}

		void laplace_select_subspace(matrix<T>& m, size_t &row, size_t &col, vector_direction& dir)
		{
			row = 0;
			col = 0;
			dir = vector_direction::bottom;
			linear_subspace sp = subspace(cartesian_coord(0, 0), vector_direction::bottom);
			int numZero = 0;
			int maxNumZero = 0;
			visit_funct count_zero = [&] (const size_t& idx) -> void { if (sp[idx] == 0) ++numZero; };

			for (size_t c=0; c<m.count_columns(); ++c, ++sp)
			{
				numZero = 0;
				sp.foreach(count_zero);

				if (numZero > maxNumZero)
				{
					maxNumZero = numZero;
					row = 0;
					col = c;
					dir = sp.get_direction();
				}
			}
			
			sp = subspace(cartesian_coord(0, 0), vector_direction::right);
			for (size_t r=0; r<m.count_rows(); ++r, ++sp)
			{
				numZero = 0;
				sp.foreach(count_zero);

				if (numZero > maxNumZero)
				{
					maxNumZero = numZero;
					row = r;
					col = 0;
					dir = sp.get_direction();
				}
			}
		}

		void swap(linear_subspace& sp1, linear_subspace& sp2, size_t length)
		{
			for (size_t s=0; s<length; ++s)
			{
				std::swap(sp1[s], sp2[s]);
			}
		}
		
		void foreach(const init_funct& f)
		{
			for (size_t row=0; row<count_rows(); ++row)
			{
				for (size_t col=0; col<count_columns(); ++col)
				{
					m_data[row][col] = f(row, col);
				}
			}
		}

		//bool find_linear_dependency(const linear_subspace& sub_1, const linear_subspace& sub_2)
		//{
		//	bool verif = false;
		//
		//	visit_funct match_f = [&] (const size_t& idx) -> void { if (sub_1[idx] == 0) ++numZero; };
		//
		//	sub_1.foreach(match_f);
		//
		//	return verif;
		//}

		//bool find_linear_dependency(const matrix<T>& m)
		//{
		//	bool linDep = false;
		//	vector_direction dir = VERTICAL_S;
		//	size_t row = 0;
		//	size_t col = 0;
		//
		//	size_t s1=0;
		//	while (s1<m.count_columns()-1 && !linDep)
		//	{
		//		linear_subspace sp1 = m.subspace(cartesian_coord(s1, 0), dir);
		//
		//		size_t s2=sp1.get_origin().x_pos+1;
		//		while (s2<m.count_columns() && !linDep)
		//		{
		//			linear_subspace sp2 = m.subspace(cartesian_coord(s2, 0), dir);
		//			linDep = find_linear_dependency(sp1, sp2);
		//			++s2;
		//		}
		//		++s1;
		//	}
		//	return linDep;
		//}

		//bool det_check_zero(const matrix<T>& m)
		//{
		//	bool detZero = false;
		//
		//	//detZero = find_linear_dependency(m);
		//
		//	return detZero;
		//}

		template<typename _RetType = T>
		_RetType laplace(const matrix<T>& m)
		{
			_RetType t = null_value<_RetType>();
			//if (!det_check_zero(m))
			{
				vector_direction direction = vector_direction::bottom;
				size_t sel_r = 0;
				size_t sel_c = 0;

				laplace_select_subspace(*this, sel_r, sel_c, direction);

				linear_subspace sub = subspace(cartesian_coord(sel_c, sel_r), direction);
				size_t R = sub.get_origin().y_pos;
				size_t C =  sub.get_origin().x_pos;

				increment_funct incr_f = [&] (size_t& sr, size_t& sc) -> void { if (direction == vector_direction::right) ++sc; else ++sr; };

				matrix<T> temp(m.count_rows()-1, m.count_columns()-1);
				for (size_t sub_s=0; sub_s < sub.get_length(); ++sub_s)
				{
					if (sub[sub_s] != null_value())
					{
						init_funct f = [&] (const size_t& r, const size_t& c) -> T { return m.m_data[(r < R) ? r : r+1][(c < C) ? c: c+1]; };
						temp.init(f);

						t += static_cast<_RetType>(std::pow(-1.0, R+C)) * static_cast<_RetType>(sub[sub_s]) * temp.det<_RetType>();
					}
					incr_f(R, C);
				}
			}

			return t;
		}
	};

	template<class T>
	std::ostream& operator<< (std::ostream &out, const matrix<T> &m)
	{
		std::stringstream ss;
		for (size_t r=0; r<m.count_rows(); ++r)
		{
			for (size_t c=0; c<m.count_columns(); ++c)
			{
				ss << m(static_cast<int>(r), static_cast<int>(c));
				if (c < m.count_columns()-1)
				{
					ss << ", ";
				}
				else
				{
					ss << "\n";
				}
			}
		}
		out << ss.str();
		return out;
	}

	namespace equations
	{
		namespace squared
		{
			template <typename T, typename _ResType>
			class system
			{
			public:

				class solution : public std::vector<_ResType>
				{
				public:
					solution() {}
					solution(size_t sz) : std::vector<_ResType>(sz) {}
					virtual ~solution(void) {}

					friend std::ostream& operator<< (std::ostream &out, system<T, _ResType>::solution& s)
					{
						std::stringstream ss;
						for (size_t i = 0; i<s.size(); ++i)
						{
							_ResType res = s[i];
							std::string var = system<T, _ResType>::variable(static_cast<const int>(i));
							ss << var << " = " << res;
							if (i < s.size() - 1)
								ss << ", ";
						}
						out << ss.str();
						return out;
					}
					/*system<T, _ResType>::solution& operator= (system<T, _ResType>::solution& s)
					{
						m_data = s.m_data;
						return (*this);
					}*/
					/*system<T, _ResType>::solution& operator= (std::vector<_ResType>& v)
					{
						m_data = v;
						return (*this);
					}*/
				};

				static char m_variables[];

				system(int vars) :
							m_coeff([this](const size_t& h, const size_t& w) -> T { return T(); }, vars, vars),
							m_res(vars)
				{
				};
				virtual ~system() {};

				int size() const { return static_cast<int>(m_coeff.count_columns()); }
				T term(const int& idx) const { return m_res[idx]; }

				T& operator()(int equation, int variable)
				{
					if (variable == m_coeff.count_columns())
						return m_res[equation];
					else
						return m_coeff(equation, variable);
				}

				solution solve()
				{
					auto d = m_coeff.det<_ResType>();

					if (d == math::matrix<T>::null_value())
						throw std::exception("The system has no solutions");

					solution sols(m_coeff.count_rows());

					for (int i = 0; i < m_coeff.count_columns(); ++i)
					{
						math::matrix<T> m(m_coeff);

						auto& s = m.subspace(i, 0, math::vector_direction::bottom);
						for (int j=0; j<s.get_length(); ++j)
							s[j] = m_res[j];

						auto ds = m.det<_ResType>();
						sols[i] = ds / d;
					}
					return sols;
				}

				bool check(solution sol)
				{
					_ResType zero = _ResType();
					_ResType total = zero;//math::matrix<T>::null_value<_ResType>();

					for (int r = 0; r < m_coeff.count_rows(); ++r)
					{
						auto& s = m_coeff.subspace(0, r, math::vector_direction::right);
						for (int i = 0; i < s.get_length(); ++i)
						{
							total = total + (static_cast<_ResType>(s[i]) * static_cast<_ResType>(sol[i]));
						}
						total -= m_res[r];
					}
					return total == zero;//math::matrix<T>::null_value<decltype(total)>();
				}

				static std::string variable(const int& index)
				{
					std::stringstream ss;
					ss << m_variables[index % 11];
					int reminder = index / 11;
					if (reminder > 0)
						ss << reminder;
					return ss.str();
				}

				friend std::ostream& operator<< (std::ostream &out, system<T, _ResType>& s)
				{
					std::stringstream ss;
					for (size_t r = 0; r<s.size(); ++r)
					{
						for (size_t c = 0; c < s.size(); ++c)
						{
							T term = s(static_cast<int>(r), static_cast<int>(c));
							std::string var = system<T, _ResType>::variable(static_cast<int>(c));
							if (c == 0)
							{
								if (term != _ResType())//math::matrix<T>::null_value<_ResType>())
									ss << term << var;
							}
							else
							{
								if (term > _ResType())//math::matrix<T>::null_value<_ResType>())
									ss << " +" << term << var;
								else if (term < _ResType())//math::matrix<T>::null_value<_ResType>())
									ss << " " << term << var;
							}
						}
						ss << " = " << s.term(static_cast<const int>(r)) << "\n";
					}
					out << ss.str();
					return out;
				}

			protected:
				math::matrix<T> m_coeff;
				std::vector<T> m_res;

				//template <typename _ZeroType = _ResType>
				//_ZeroType zero() { return decltype(m_coeff)::null_value<_ZeroType>(); }
			};

			template <typename T, typename _ResType>
			char system<T, _ResType>::m_variables[] = { 'x', 'y', 'z' , 't' , 'u' , 'v' , 'w' , 'p' , 'q' , 'r' , 's'};
		}
	}
}