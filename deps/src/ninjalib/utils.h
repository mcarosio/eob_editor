#pragma once



#include <string>
#include <chrono>
#include <type_traits>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <experimental/filesystem>
#include <cstdint>
#include <iomanip>
//#include <limits>
//#include <future>
#include <thread>
#include <map>
#include <set>
#include <fstream>
#include <functional>
#include <atomic>
#include <list>
#include <utility>
#include <tuple>
//#include <boost/algorithm/string.hpp>


//#ifdef _WIN32
//	#ifdef _WINDLL
//		#ifdef NINJALIB_EXPORTS  
//			#define NINJALIB_LIBRARY_API __declspec(dllexport)   
//		#else  
//			#define NINJALIB_LIBRARY_API __declspec(dllimport)   
//		#endif
//	#else
//		#define NINJALIB_LIBRARY_API
//	#endif
//#endif

namespace utils
{
	template<typename T>
	class defaultable
	{
	public:
		defaultable(const T& defaultValue)
			: m_default(defaultValue), m_value(defaultValue), m_hasValue(false)
		{
		}
		virtual ~defaultable() {}

		const T& value() const { return m_value; }
		const T& default_value() const { return m_default; }
		void reset() { m_value = m_default; }
		bool has_value() const { return m_hasValue; }
		operator bool() const { return has_value(); }
		operator const T&() const { return value(); }
		
		template <typename _RetType = T>
		_RetType as() const
		{
			std::stringstream ss;
			ss << m_value;
			_RetType r = _RetType();
			ss >> r;
			return r;
		}

		defaultable& operator= (const T& val)
		{
			m_hasValue = m_value != val;
			m_value = val;
			return (*this);
		}
		defaultable& operator= (const defaultable& d)
		{
			m_hasValue = m_value != d.value();
			m_value = d.value();
			return (*this);
		}
		bool operator== (const T& val) const
		{
			return m_value == val;
		}
		bool operator== (const defaultable& d) const
		{
			return m_value == d.value();
		}
		friend std::ostream& operator<< (std::ostream &out, const defaultable& d)
		{
			out << d.value();
			return out;
		}
		friend std::istream& operator>> (std::istream& input, utils::defaultable<T>& d)
		{
			T val;
			input >> val;
			d = val;
			return input;
		}

	protected:
		T		m_value;
		T		m_default;
		bool	m_hasValue;
	};

	class ascii_utils
	{
	public:

		enum class justify
		{
			left,
			right
		};

		ascii_utils(void) {};
		virtual ~ascii_utils(void) {};

		template <class T>
		static utils::defaultable<T> parse(const char *ascii, size_t length, T defaultValue)
		{
			utils::defaultable<T> val(defaultValue);
			std::stringstream s(std::string(ascii, length));
			if (length > 0)
				s >> val;
			return val;
		}

		template <>
		static utils::defaultable<std::string> parse(const char *ascii, size_t length, std::string defaultValue)
		{
			utils::defaultable<std::string> val(defaultValue);
			if (length > 0)
				val = std::string(ascii, length);
			return val;
		}
		
		template <class T, bool zeroTerminate = false>
		static void write(char* text, size_t textLength, const T& t, char padding = ' ', justify align = justify::right)
		{
			auto inputLength = (zeroTerminate) ? textLength-1 : textLength;
			std::stringstream s;
			s << std::setw(inputLength) << std::setfill(padding) << ((align == justify::right) ? std::right : std::left) << t;
			memcpy_s(text, textLength, s.str().c_str(), inputLength);
			if (zeroTerminate)
				text[inputLength] = 0;
		}
		
		template <class T>
		static void write(std::string &text, const T& t, char padding = ' ', justify align = justify::right)
		{
			std::stringstream s;
			s << t;
			text = s.str();
		}

		void copy_text(const char *source, size_t sourceSize, char *dest, size_t destSize);
	};

	class string_utils
	{
	public:

		class string_builder
		{
		public:
			string_builder(void) {}
			string_builder(const std::string& text) : m_buffer(text) {}
			virtual ~string_builder(void) {}

			std::locale imbue(const std::locale& loc) { return m_buffer.imbue(loc); }
			template <class T>
			void push(T data) { m_buffer << data; }
			void clear() { m_buffer.str(""); }

			string_builder& operator<< (const std::string& text)
			{
				m_buffer << text;
				return (*this);
			}
			friend std::ostream& operator<< (std::ostream& out, const string_builder& builder)
			{
				out << builder.m_buffer.str();
				return out;
			}
			operator const char* ()
			{
				return this->m_buffer.str().c_str();
			}

		protected:
			std::stringstream m_buffer;
		};

		class kmp_matcher
		{
		public:

			enum class matcher_input_source
			{
				none,
				string,
				file
			};

			struct matcher_input
			{
				matcher_input()
				{
					m_source = matcher_input_source::none;
				}
				matcher_input(const char* input)
				{
					set_source(input);
				}
				matcher_input(const std::string& input)
				{
					set_source(input);
				}
				matcher_input(std::ifstream& input)
				{
					set_source(input);
				}
				matcher_input(const std::experimental::filesystem::path& input)
				{
					set_source(input);
				}

				matcher_input& operator= (const std::experimental::filesystem::path& input)
				{
					set_source(input);
					return (*this);
				}
				matcher_input& operator= (std::ifstream& input)
				{
					set_source(input);
					return (*this);
				}
				matcher_input& operator= (const std::string &input)
				{
					set_source(input);
					return (*this);
				}
				matcher_input& operator= (const char* input)
				{
					set_source(input);
					return (*this);
				}

				char operator[] (long long idx)
				{
					return get(idx);
				}

				char get(long long offset);

				const matcher_input_source& get_source() const { return m_source; }

				size_t length() const
				{
					if (m_source == matcher_input_source::file)
						return static_cast<size_t>(m_fileSize);
					else if (m_source == matcher_input_source::string)
						return m_string.length();
					else if (m_source == matcher_input_source::none)
						return 0;
					else
						throw std::exception("Input source not implemented");
				}

				const std::string& str() const
				{
					if (m_source != matcher_input_source::string)
						throw std::exception("String source is not available");
					return m_string;
				}
				const std::ifstream& file() const
				{
					if (m_source != matcher_input_source::file)
						throw std::exception("File source is not available");
					return m_file;
				}
				friend std::ostream& operator<< (std::ostream &out, const matcher_input& m)
				{
					std::stringstream ss;
					if (m.m_source == matcher_input_source::file)
					{
						ss << "File input";
					}
					else if (m.m_source == matcher_input_source::string)
					{
						ss << m.str();
					}
					else if (m.m_source == matcher_input_source::none)
					{
						ss << "(Null)";
					}
					else
					{
						throw std::exception("Input source not implemented");
					}
					out << ss.str();
					return out;
				}

			protected:
				void set_source(const char* input)
				{
					m_string = input;
					m_source = matcher_input_source::string;
				}
				void set_source(const std::string& input)
				{
					set_source(input.c_str());
				}
				void set_source(std::ifstream& input)
				{
					m_file = std::move<std::ifstream&>(input);
					if (!m_file.is_open())
						throw std::exception("Input source not ready");
					if (m_file.bad())
						throw std::exception("Input source not valid");
					m_source = matcher_input_source::file;

					m_file.seekg(0, m_file.end);
					m_fileSize = m_file.tellg();
					m_file.seekg(0, m_file.beg);
				}
				void set_source(const std::experimental::filesystem::path& input)
				{
					if (!std::experimental::filesystem::exists(input))
						throw std::exception("Input path does not exist");
					m_file.open(input.string(), std::ios::in);
					if (!m_file.is_open())
						throw std::exception("Input source not ready");
					if (m_file.bad())
						throw std::exception("Input source not valid");
					m_source = matcher_input_source::file;

					m_file.seekg(0, m_file.end);
					m_fileSize = m_file.tellg();
					m_file.seekg(0, m_file.beg);
				}

			private:
				std::streampos m_fileSize;
				std::ifstream m_file;
				std::string m_string;
				matcher_input_source m_source;
			};

			kmp_matcher(void) {}
			virtual ~kmp_matcher(void) {}

			void search(matcher_input& text, const std::string& pattern, bool ignoreCase = false)
			{
				search(text, pattern.c_str(), pattern.length(), ignoreCase);
			}
			void search(matcher_input& text, const char* pattern, const size_t& patternLenght, bool ignoreCase = false);

			auto count() { return m_occurrences.size(); }
			auto begin() { return m_occurrences.begin(); }
			auto end() { return m_occurrences.end(); }
			auto cbegin() { return m_occurrences.cbegin(); }
			auto cend() { return m_occurrences.cend(); }

		protected:
			void init_pattern_prefix(const char* pattern, const size_t& patternLength, bool ignoreCase = false);

			template<bool ignoreCase = false>
			int match_char(const char& a, const char& b)
			{
				return (ignoreCase) ? std::tolower(static_cast<int>(b), std::locale()) - std::tolower(static_cast<int>(a), std::locale()) : static_cast<int>(b) - static_cast<int>(a);
			}

			int match_char(const char& a, const char& b, bool ignoreCase)
			{
				return (ignoreCase) ? match_char<true>(a, b) : match_char<false>(a, b);
			}

		private:
			std::vector<int> m_prefix;
			std::list<int> m_occurrences;
		};

		class string_pool
		{
		public:
			~string_pool(void) {};
			static std::shared_ptr<string_pool> get_instance()
			{
				static std::shared_ptr<string_pool> s{ new string_pool };
				return s;
			}
			const char* const get(std::string& str)
			{
				return get(str.c_str());
			}
			const char* const get(const char* str)
			{
				if (m_pool.find(str) == m_pool.end())
					m_pool.emplace(str);
				auto p = m_pool.find(str);
				return (*p).c_str();
			}

			string_pool(string_pool const&) = delete;
			void operator=(string_pool const&) = delete;

		private:
			std::set<std::string> m_pool;

			string_pool(void) {};
		};

		struct case_insensitive_char_traits : public std::char_traits<char>
		{
			static bool eq(char c1, char c2) { return toupper(c1) == toupper(c2); }
			static bool ne(char c1, char c2) { return toupper(c1) != toupper(c2); }
			static bool lt(char c1, char c2) { return toupper(c1) <  toupper(c2); }

			static int compare(const char* s1, const char* s2, size_t n)
			{
				while(n-- != 0)
				{
					if (toupper(*s1) < toupper(*s2)) return -1;
					if (toupper(*s1) > toupper(*s2)) return 1;
					++s1;
					++s2;
				}
				return 0;
			}
			static const char* find(const char* s, int n, char a)
			{
				while (n-- > 0 && toupper(*s) != toupper(a))
				{
					++s;
				}
				return s;
			}
		};

		struct facet_struct : std::numpunct<char>
		{
			facet_struct(char sep, char dec) : _sep(sep), _dec(dec) {}
			char do_thousands_sep() const { return _sep; }
			char do_decimal_point() const { return _dec; }
			std::string do_grouping() const
			{
				if (_sep == 0) return std::numpunct<char>::do_grouping();
				else return "\3";
			}

			char _sep;
			char _dec;
		};

		string_utils(void) {};
		virtual ~string_utils(void) {};

		static void ltrim(std::string &s);
		static void rtrim(std::string &s);
		static void trim(std::string &s);
		static void remove_quote(std::string &s);
		template <class T>
		static bool split(const std::string& text, const std::string& delimiter, std::vector<T>& tokens)
		{
			size_t pos = 0;
			size_t start = 0;
			std::string token;

			tokens.clear();
			if (text.empty())
				return false;
			while ((pos = text.find(delimiter, start)) != std::string::npos)
			{
				token = text.substr(start, pos-start);
				T v = utils::ascii_utils::parse<T>(token.c_str(), token.length(), T());
				tokens.push_back(v);
				pos += delimiter.length();
				start = pos;
			}
			if (start < text.size())
			{
				token = text.substr(start, text.size()-start);
				T v = utils::ascii_utils::parse<T>(token.c_str(), token.length(), T());
				tokens.push_back(v);
			}
			return true;
		}

		static int compare_no_case(const std::string &a, const std::string &b);
		static std::string format_size(std::uint64_t size, int prec = 2, char thousandsSeparator = 0, char decimalSeparator = '.');
		
		template <typename T>
		static std::string format_number(T number, int prec = 2, char thousandsSeparator = 0, char decimalSeparator = '.')
		{
			string_utils::facet_struct *_facet = new string_utils::facet_struct(thousandsSeparator, decimalSeparator);

			std::stringstream ss(std::stringstream::in | std::stringstream::out);
			ss.imbue(std::locale(std::locale(), _facet));
			ss << std::fixed << std::setprecision(prec) << number;
			return ss.str();
		}

		static long replace_all(std::string& text, const std::string& pattern, const std::string& newText);
	};

	class time_utils
	{
	public:

		class timer
		{
		private:
			std::chrono::time_point<std::chrono::high_resolution_clock> m_beg;
 
		public:
			timer() : m_beg(std::chrono::high_resolution_clock::now()) {}
	
			void reset() { m_beg = std::chrono::high_resolution_clock::now(); }
	
			template<typename _RetType = long long>
			_RetType nano() const { return std::chrono::duration_cast<std::chrono::duration<_RetType, std::nano>>(std::chrono::high_resolution_clock::now() - m_beg).count(); }
			template<typename _RetType = long long>
			_RetType micro() const { return std::chrono::duration_cast<std::chrono::duration<_RetType, std::micro>>(std::chrono::high_resolution_clock::now() - m_beg).count(); }
			template<typename _RetType = long long>
			_RetType milli() const { return std::chrono::duration_cast<std::chrono::duration<_RetType, std::milli>>(std::chrono::high_resolution_clock::now() - m_beg).count(); }
			template<typename _RetType = long long>
			_RetType sec() const { return milli() / 1000; }
		};

		time_utils(void) {};
		virtual ~time_utils(void) {};
		
		static std::tm get_current_time();
		static std::string get_current_time(const char *timeFormat);
		static std::string get_current_time(const std::time_t& t, const char *timeFormat);
		static void wait(long milliseconds);
	};

	class file_utils
	{
	public:

		enum class file_type
		{
			pk_zip,
			java_class,
			gif87a,
			gif89a,
			unix_batch,
			elf,
			pdf,
			mbr,
			utf16_big_endian,
			utf16_little_endian,
			amiga_disk_format
		};

		using reader_progress = std::function<void(const size_t& linesRead, const size_t& currentSize, const size_t& totalSize, const double& percentage)>;

		enum class progress_notification_mode
		{
			percentage = 0,
			line_number,
			data_processed
		};

		class lines_iterator
		{
			public:
				lines_iterator(const std::string& path, const char delim = '\n',
					const reader_progress& cbk = nullptr,
					const progress_notification_mode& mode = progress_notification_mode::percentage,
					const size_t freq = 1)
					: m_fullPath(path), m_index(1), m_in(path, std::ios::in), m_delim(delim),
					m_progressCbk(cbk), m_notifyMode(mode), m_notifyFreq(freq),
					m_nextNotified(freq)
				{
					m_in.seekg(0, std::ios::end);
					m_fileSize = static_cast<size_t>(m_in.tellg());
					m_in.seekg(0, std::ios::beg);
					std::memset((void*)m_buffer, 0, sizeof(m_buffer));

					m_in.getline(m_buffer, sizeof(m_buffer), m_delim);
					check_progress();
				}
				lines_iterator()
					: m_fullPath(""), m_index(0), m_delim('\n'),
					m_progressCbk(nullptr), m_notifyMode(progress_notification_mode::percentage), m_notifyFreq(1),
					m_nextNotified(0)
				{
					m_fileSize = 0;
					std::memset((void*)m_buffer, 0, sizeof(m_buffer));
				}
				lines_iterator(const lines_iterator& it)
					: m_fullPath(it.m_fullPath), m_index(it.m_index),
					m_in(it.m_fullPath.string(), std::ios::in),
					m_delim(it.m_delim), m_fileSize(it.m_fileSize),
					m_progressCbk(it.m_progressCbk), m_notifyMode(it.m_notifyMode), m_notifyFreq(it.m_notifyFreq),
					m_nextNotified(it.m_nextNotified)
				{
					std::memset((void*)m_buffer, 0, sizeof(m_buffer));
				}
				
				~lines_iterator(){}

				lines_iterator& operator=(const lines_iterator& it) = default;

				operator bool()const
				{
					return !m_in.eof();
				}

				bool operator==(const lines_iterator& it) const
				{
					return (m_index == it.m_index);
				}
				bool operator!=(const lines_iterator& it) const
				{
					return (m_index != it.m_index);
				}
				lines_iterator& operator++()
				{
					m_in.getline(m_buffer, sizeof(m_buffer), m_delim);
					if (m_in.eof())
						m_index = 0;
					else
					{
						++m_index;
						check_progress();
					}
					return (*this);
				}
				const char* operator*()
				{
					return m_buffer;
				}
				size_t get_index() const
				{
					return m_index;
				}
				size_t pos()
				{
					return static_cast<size_t>(m_in.tellg());
				}
				double perc()
				{
					return static_cast<double>(pos()) / m_fileSize;
				}
					
			protected:
				void check_progress();

				size_t										m_index;
				size_t										m_fileSize;
				std::experimental::filesystem::path			m_fullPath;
				std::ifstream								m_in;
				char										m_buffer[65536];
				char										m_delim;
				reader_progress								m_progressCbk;
				progress_notification_mode					m_notifyMode;
				size_t										m_notifyFreq;
				size_t										m_nextNotified;
		};

		file_utils(void) {};
		virtual ~file_utils(void) {};

		static std::experimental::filesystem::path version_file(const std::experimental::filesystem::path& p);
		static std::experimental::filesystem::path version_file(const std::experimental::filesystem::path& p, const time_t& ts);

		static file_utils::lines_iterator begin(const std::string& path, const char delim = '\n',
			const reader_progress& cbk = nullptr,
			const progress_notification_mode& mode = progress_notification_mode::percentage,
			const size_t freq = 1)
		{
			return lines_iterator::lines_iterator(path, delim, cbk, mode, freq);
		}
		static file_utils::lines_iterator end()
		{
			return lines_iterator::lines_iterator();
		}

		static std::time_t last_write_time(const std::experimental::filesystem::path &p);
		
		static bool check_magic_number(const std::experimental::filesystem::path &filePath, const file_type& type);
		static bool check_magic_number(std::ifstream &inFile, const file_type& type);
		static bool check_magic_number(const uint8_t *buffer, const file_type& type);

		static const uint8_t* const magic_number(const file_type& type);
		static const uint8_t const magic_number_size(const file_type& type);

	protected:
		static const uint8_t m_magicNumbers[11][8];
		static const uint8_t m_magicNumberSizes[];
	};

	class compare_utils
	{
	public:
		
		enum class compare_direction { ascending, descending };

		compare_utils(void) : m_dir(compare_direction::ascending) {};
		compare_utils(const compare_direction& dir) : m_dir(dir) {};
		virtual ~compare_utils(void) {};
	
		template <class T>
		int compare(const T& a, const T& b) { return compare(a, b, m_dir); }

		template <class T>
		static int compare(const T& a, const T& b, const compare_direction& dir)
		{
			if (dir == compare_direction::ascending)
				if (a < b) return -1;
				else if (a > b) return 1;
				else return 0;
			else return -compare<T>(a, b, compare_direction::ascending);
		}
		template <>
		static int compare(const std::string& a, const std::string& b, const compare_direction& dir)
		{
			if (dir == compare_direction::ascending)
				return utils::string_utils::compare_no_case(a, b);
			else return compare<std::string>(b, a, compare_direction::ascending);
		}

	protected:
		compare_direction m_dir;
	};

	class progress
	{
	public:

		struct stats
		{
			stats(long long size)
			{
				batch_size = size;
				current_point = 0;
				percentage_done = 0.0;
				time_started = 0;
				time_elapsed = 0;
				estimated_total = 0;
				estimated_remaining = 0;
			}

			long long batch_size;
			long long current_point;
			double percentage_done;
			std::time_t time_started;
			std::time_t time_elapsed;
			long long estimated_total;
			long long estimated_remaining;
		};
		
		progress(long long batch_size)
			: m_stats(batch_size)
		{};
		progress()
			: m_stats(0)
		{};
		virtual ~progress(void) {};

		progress& operator+=(const unsigned int& movement)
		{
			move(movement);
			return (*this);
		}

		void set_batch_size(long long batch_size)
		{
			m_stats.batch_size = batch_size;
		}
		const long long& get_batch_size()
		{
			return m_stats.batch_size;
		}
		bool done()
		{
			return m_stats.percentage_done >= 1.0;
		}

		void start()
		{
			m_timer.reset();
			m_stats.time_started = m_timer.milli();
		}
		stats& get_statistics() { return m_stats; }

	protected:
		stats				m_stats;
		time_utils::timer	m_timer;

		bool move(const unsigned int& movement);
	};

	class calendar_utils
	{
	public:
		calendar_utils() {};
		virtual ~calendar_utils() {};

		static time_t easter_date(int year);

	private:
		static const int M;
		static const int N;
		static const int D1;
		static const int D2;
		static const int D3;
	};

	class version
	{
	public:

		enum class version_info { comments = 0, internal_name, product_name, company_name
					,legal_copyright, product_version, file_description
					,legal_trademarks, private_build, file_version
					,original_filename, special_build
		};

		enum class index { major_number = 0, minor_number, maintenance_number, build_number };

		version(const std::string& maj, const std::string& min, const std::string& mnt = "0", const std::string& bld = "0")
		{
			m_vers.push_back(maj);
			m_vers.push_back(min);
			m_vers.push_back(mnt);
			m_vers.push_back(bld);
			for (int idx = 0; idx < m_vers.size(); ++idx)
				string_utils::trim(m_vers[idx]);
		}
		version(const std::string& v, const char& sep)
		{
			std::vector<std::string> data;
			std::stringstream ss;
			ss << sep;
			string_utils::split<std::string>(v, ss.str(), data);

			m_vers.resize(4, "");
			for (int idx = 0; idx < data.size(); ++idx)
			{
				m_vers[idx] = data[idx];
				string_utils::trim(m_vers[idx]);
			}
		}
		version(const int& maj, const int& min, const int& mnt = 0, const int& bld = 0)
		{
			m_vers.resize(4, "0");
			ascii_utils::write<int>(m_vers[0], maj, ' ', ascii_utils::justify::left);
			ascii_utils::write<int>(m_vers[1], min, ' ', ascii_utils::justify::left);
			ascii_utils::write<int>(m_vers[2], mnt, ' ', ascii_utils::justify::left);
			ascii_utils::write<int>(m_vers[3], bld, ' ', ascii_utils::justify::left);
			for (int idx = 0; idx < m_vers.size(); ++idx)
				string_utils::trim(m_vers[idx]);
		};
		virtual ~version() {};

		bool operator==(const version& v) const
		{
			return (major() == v.major()
					&& minor() == v.minor()
					&& maintenance() == v.maintenance()
					&& build() == v.build());
		}

		operator std::string()
		{
			std::stringstream ss;
			ss << (*this);
			return ss.str();
		}

		bool operator<(const version& v) const
		{
			short idx = 0;
			while (idx < m_vers.size())
			{
				if (m_vers[idx] < v.m_vers[idx])
					return true;
				else if (m_vers[idx] > v.m_vers[idx])
					return false;
				else
					++idx;
			}
			return false;
		}

		bool operator<=(const version& v) const
		{
			return (*this) < v || (*this) == v;
		}

		bool operator>(const version& v) const
		{
			return !((*this) <= v);
		}

		const std::string& major() const
		{
			return m_vers[static_cast<int>(index::major_number)];
		}
		const std::string& minor() const
		{
			return m_vers[static_cast<int>(index::minor_number)];
		}
		const std::string& maintenance() const
		{
			return m_vers[static_cast<int>(index::maintenance_number)];
		}
		const std::string& build() const
		{
			return m_vers[static_cast<int>(index::build_number)];
		}

		const std::string& operator[] (const index idx) const
		{
			if (static_cast<int>(idx) < 0 || idx > index::build_number)
			{
				throw std::exception("Invalid index");
			}

			return m_vers[static_cast<int>(idx)];
		}

		version& operator= (const version &v)
		{
			m_vers = v.m_vers;
			return (*this);
		}

		version& operator= (const std::string &v)
		{
			string_utils::split<std::string>(v, ".", m_vers);
			for (int idx = 0; idx < m_vers.size(); ++idx)
				string_utils::trim(m_vers[idx]);
			return (*this);
		}

		friend std::ostream& operator<< (std::ostream &out, const version& v)
		{
			std::stringstream ss;
			for (int idx=0; idx<v.m_vers.size(); ++idx)
			{
				if (idx > 0 && !v.m_vers[idx].empty())
					ss << ".";
				ss << v.m_vers[idx];
			}
			out << ss.str();
			return out;
		}

#ifdef _WIN32
		struct win
		{
			static std::string query(const version::version_info& vi);
		};
#endif

	private:
		static const char* version_info_names[];
		std::vector<std::string> m_vers;
	};

	template<typename _RetType, typename ..._Types>
	class async_runner
	{
	public:

		using task_function = std::function<_RetType(_Types... t)>;
		using task_callback = std::function<void(const _RetType&)>;

		async_runner(const task_function& fn,
					const task_callback& endCbk = nullptr,
					int delay = 0)
			: m_fn(fn), m_end(endCbk), m_delay(delay), m_busy(false), m_result(_RetType())
		{};
		virtual ~async_runner() {};

		void run(_Types&&... t)
		{
			m_busy = true;
			auto do_work = [&](const time_utils::timer& timer) -> void
			{
				if (m_delay > 0)
					std::this_thread::sleep_for(std::chrono::milliseconds(m_delay));

				m_result = std::forward<task_function>(m_fn)(std::forward<_Types>(t)...);

				m_busy = false;
				m_elapsed = timer.milli<long long>();

				if (m_end)
					m_end(m_result);
			};
			time_utils::timer timer;
			timer.reset();
			m_thread = new std::thread(do_work, timer);
		}
		
		void wait()
		{
			m_thread->join();
			delete m_thread;
		}

		_RetType result()
		{
			return m_result;
		}

		bool is_busy()
		{
			return m_busy;
		}

		long long time_elapsed()
		{
			return m_elapsed;
		}

	protected:
		task_function			m_fn;
		task_callback			m_end;
		int						m_delay;
		std::thread				*m_thread;
		std::atomic<bool>		m_busy;
		_RetType				m_result;
		long long				m_elapsed;
	};
	


	namespace tuple_helper
	{
		template<class Tuple, std::size_t N>
		struct tuple_printer
		{
			static void print(std::ostream &out, const Tuple& t)
			{
				tuple_printer<Tuple, N - 1>::print(out, t);
				out << ", " << std::get<N - 1>(t);
			}
		};

		template<class Tuple>
		struct tuple_printer<Tuple, 1>
		{
			static void print(std::ostream &out, const Tuple& t) { out << std::get<0>(t); }
		};

		template<const char beginningChar, const char endingChar, class... Args>
		void print(std::ostream &out, const std::tuple<Args...>& t)
		{
			out << beginningChar;
			tuple_printer<decltype(t), sizeof...(Args)>::print(out, t);
			out << endingChar;
		}
	}



	template<typename ..._Types>
	class record
	{
	public:
		record() {}
		record(const std::tuple<_Types...>& t) : m_items(t) {}
		virtual ~record() {}

		friend std::ostream& operator<< (std::ostream &out, const record& r)
		{
			tuple_helper::print<'<', '>', _Types...>(out, r.m_items);
			return out;
		}
		record& operator= (const std::tuple<_Types...> &v)
		{
			m_items = v;
			return (*this);
		}
		record& operator= (std::tuple<_Types...> v)
		{
			m_items = v;
			return (*this);
		}
		record& operator= (const record &r)
		{
			m_items = r.m_items;
			return (*this);
		}
		bool operator==(const record& r) const
		{
			return (m_items == r.m_items);
		}
		bool operator==(const std::tuple<_Types...>& r) const
		{
			return (m_items == r);
		}
		template<size_t idx>
		auto get()
		{
			return std::get<idx>(m_items);
		}

	protected:

		std::tuple<_Types...> m_items;
	};



	template<typename ..._Types>
	class sequential_collection
	{
	public:
		sequential_collection() : m_captions(sizeof...(_Types)) {}
		virtual ~sequential_collection() {}

			void add(_Types... t)
			{
				record<_Types...> r = std::tuple<_Types...>(t...);
				m_data.push_back(r);
			}
		auto& first()
		{
			return m_data.front();
		}
		auto& last()
		{
			return m_data.back();
		}
		friend std::ostream& operator<< (std::ostream &out, const sequential_collection<_Types...>& set)
		{
			out << "{";
			int count = 0;
			for(auto& t : set.m_data)
			{
				if (count > 0) out << ',';
				out << t;
				++count;
			}
			out << "}";
			return out;
		}
		size_t size()
		{
			return m_data.size();
		}
		auto begin() { return m_data.begin(); }
		auto end() { return m_data.end(); }

		std::string& caption(const int& position)
		{
			if (position < 0 || position >= m_captions.size())
				throw std::exception("Invalid column index");

			return m_captions[position];
		}

	protected:
		std::list<record<_Types...>>	m_data;
		std::vector<std::string>		m_captions;
	};



	template<typename _Key, typename ..._Types>
	class random_collection
	{
	public:
		random_collection() : m_captions(sizeof...(_Types)) {}
		virtual ~random_collection() {}

		void add(const _Key& k, _Types... t)
		{
			record<_Types...> r = std::tuple<_Types...>(t...);
			m_data[k] = r;
		}
		auto at(const _Key& k)
		{
			return m_data[k];
		}
		record<_Types...>& operator[] (const _Key& k)
		{
			return m_data[k];
		}
		friend std::ostream& operator<< (std::ostream &out, const random_collection<_Key, _Types...>& set)
		{
			out << "{";
			int count = 0;
			for (auto& t : set.m_data)
			{
				if (count > 0) out << ',';
				out << "[" << t.first << "] " << t.second;
				++count;
			}
			out << "}";
			return out;
		}
		friend std::ostream& operator<< (std::ostream &out, const std::pair<_Key, record<_Types...>>& pair)
		{
			out << "{";
			out << "[" << pair.first << "] " << pair.second;
			out << "}";
			return out;
		}
		size_t size()
		{
			return m_data.size();
		}
		auto begin() { return m_data.begin(); }
		auto end() { return m_data.end(); }

		std::string& caption(const int& position)
		{
			if (position < 0 || position >= m_captions.size())
				throw std::exception("Invalid column index");

			return m_captions[position];
		}

	protected:
		std::map<_Key, record<_Types...>>	m_data;
		std::vector<std::string>			m_captions;
	};

	template<typename ..._Types>
	class indexed_collection
	{
	public:
		indexed_collection() : m_captions(sizeof...(_Types)) {}
		indexed_collection(int initialSize) : m_data(initialSize) {}
		virtual ~indexed_collection() {}

		void add(const int& idx, _Types... t)
		{
			record<_Types...> r = std::tuple<_Types...>(t...);
			if (idx >= m_data.size())
				m_data.resize(idx+1);
			m_data[idx] = r;
		}
		auto at(const int& k)
		{
			return m_data[k];
		}
		record<_Types...>& operator[] (const int& k)
		{
			return m_data[k];
		}
		friend std::ostream& operator<< (std::ostream &out, const indexed_collection<_Types...>& set)
		{
			out << "{";
			int count = 0;
			for (auto& t : set.m_data)
			{
				if (count > 0) out << ',';
				out << t;
				++count;
			}
			out << "}";
			return out;
		}
		size_t size()
		{
			return m_data.size();
		}
		auto begin() { return m_data.begin(); }
		auto end() { return m_data.end(); }

		std::string& caption(const int& position)
		{
			if (position < 0 || position >= m_captions.size())
				throw std::exception("Invalid column index");

			return m_captions[position];
		}

	protected:
		std::vector<record<_Types...>>	m_data;
		std::vector<std::string>		m_captions;
	};



	namespace fixed_ascii
	{
		template<size_t... _sizes>
		class data_record
		{
		public:

			using transform_data = std::function<std::string(const int& idx, const char* input, const int& length)>;

			data_record() : m_textLength(0), m_asciiText(nullptr), m_transient(false)
			{
				constexpr size_t x_array[] = { _sizes... };
				std::size_t offset = 0;
				for (int i = 0; i < sizeof...(_sizes); ++i)
				{
					m_sizes[i] = x_array[i];
					m_offsets[i] = offset;
					offset += m_sizes[i];
					m_transforms[i] = nullptr;
				}
				m_textLength = offset;
			}
			virtual ~data_record()
			{
				if (!m_transient)
					delete[] m_asciiText;
			}

			bool& transient() { return m_transient; }
			void set_transformation(const int& idx, const transform_data& tFunct) { m_transforms[idx] = tFunct; }

			data_record& operator= (const char* input)
			{
				if (m_transient)
				{
					m_asciiText = (char*)input;
				}
				else
				{
					if (!m_asciiText)
					{
						m_asciiText = new char[m_textLength + 1];
						memset(m_asciiText, ' ', m_textLength);
						m_asciiText[m_textLength] = 0;
					}

					std::size_t inputLength = std::min<std::size_t>(m_textLength, std::strlen(input));
					errno_t err = memcpy_s(m_asciiText, m_textLength, input, inputLength);
					if (err != 0)
						throw std::exception("Invalid input length");
				}
				return (*this);
			}
			const char* operator() (const int& idx, int& length)
			{
				length = static_cast<int>(m_sizes[idx]);
				return static_cast<const char*>(m_asciiText + m_offsets[idx]);
			}
			operator const char*()
			{
				return static_cast<const char*>(m_asciiText);
			}
			friend std::ostream& operator<< (std::ostream& out, data_record& dr)
			{
				out << static_cast<const char*>(dr);
				return out;
			}
			data_record& operator<< (const char* input)
			{
				if (m_transient)
					std::exception("Invalid operator << with transient data record");
				m_bufferedInput  << input;
				if (m_bufferedInput.str().rfind('\n') != std::string::npos)
				{
					(*this) = m_bufferedInput.str().c_str();
					m_bufferedInput.clear();
					m_bufferedInput.str(std::string());
				}
				return (*this);
			}

			std::size_t count_fields() { return sizeof...(_sizes); }
			std::string str(const int& idx, bool trim=false)
			{
				int len = 0;
				const char* field = (*this)(idx, len);
				std::string ret(field, len);
				if (trim)
					string_utils::rtrim(ret);
				return ret;
			}

			template<typename _RetType>
			_RetType as(const int& idx)
			{
				std::stringstream ss;
				_RetType t = _RetType();
				ss << str(idx);
				ss >> t;
				return t;
			}

			template<>
			std::string as(const int& idx) { return str(idx); }

			template<>
			const char* as(const int& idx)
			{
				int len = 0;
				return (*this)(idx, len);
			}

			const std::size_t& field_size(const size_t& idx)
			{
				return m_sizes[static_cast<int>(idx)];
			}

			std::string split(const char separator = '|', bool trim = false)
			{
				std::stringstream ss;
				for (size_t fieldCount = 0; fieldCount < count_fields(); ++fieldCount)
				{
					if (fieldCount > 0)
						ss << separator;
					int len = 0;
					const char* field = (*this)(static_cast<int>(fieldCount), len);
					if (trim)
					{
						while (std::isspace<char>(field[len-1], std::locale()))
						{
							--len;
						}
					}
					if (m_transforms[fieldCount] != nullptr)
					{
						std::string transField = m_transforms[fieldCount](static_cast<int>(fieldCount), field, len);
						ss << transField;
					}
					else
					{
						for (int i = 0; i < len; ++i)
							ss << field[i];
					}
				}
				return ss.str();
			}

		protected:
			std::size_t					m_sizes[sizeof...(_sizes)];
			std::size_t					m_offsets[sizeof...(_sizes)];
			char*						m_asciiText;
			std::size_t					m_textLength;
			bool						m_transient;
			transform_data				m_transforms[sizeof...(_sizes)];
			std::stringstream			m_bufferedInput;
		};
	};

	template <class T>
	class observable
	{
	public:

		enum class observable_event
		{
			before_change,
			after_change
		};

		enum class trigger_event
		{
			sequential,
			parallel
		};

		struct on_value_changed
		{
		public:
			on_value_changed(const std::function<void(const T& precValue, const T& newValue, const observable_event& evtType)>& f)
			{
				(*this) = f;
			}
			on_value_changed & operator=(const std::function<void(const T& precValue, const T& newValue, const observable_event& evtType)>& f)
			{
				m_funct = f;
				return (*this);
			}
			on_value_changed & operator() (const T& precValue, const T& newValue, const observable_event& evtType)
			{
				m_funct(precValue, newValue, evtType);
				return (*this);
			}
			operator std::function<void(const T& precValue, const T& newValue, const observable_event& evtType)>()
			{
				return m_funct;
			}
			std::uintptr_t id() const
			{
				return reinterpret_cast<std::uintptr_t>(this);
			}
		protected:
			std::function<void(const T& precValue, const T& newValue, const observable_event& evtType)> m_funct;
		};

		observable() : m_value(), m_evtTrigger(utils::observable<T>::trigger_event::parallel)
		{
		}

		observable(T value) : m_value(value), m_evtTrigger(utils::observable<T>::trigger_event::parallel)
		{
		}
		~observable() {}

		observable& operator= (const T& newValue)
		{
			if (trigger() == trigger_event::parallel)
			{
				for (auto& c : m_cbks)
					c.second(m_value, newValue, observable_event::before_change);
				m_value = newValue;
				for (auto& c : m_cbks)
					c.second(m_value, newValue, observable_event::after_change);
			}
			else if (trigger() == trigger_event::sequential)
			{
				T oldValue = m_value;
				for (auto& c : m_cbks)
				{
					c.second(oldValue, newValue, observable_event::before_change);
					m_value = newValue;
					c.second(oldValue, newValue, observable_event::after_change);
				}
			}
			return (*this);
		}
		observable& operator+= (const on_value_changed& cbk)
		{
			m_cbks.emplace(cbk.id(), cbk);
			return (*this);
		}
		observable& operator-= (const on_value_changed& cbk)
		{
			auto id = cbk.id();
			m_cbks.erase(id);
			return (*this);
		}
		friend std::ostream& operator<< (std::ostream& out, observable& obj)
		{
			out << obj.m_value;
			return out;
		}
		auto count()
		{
			return m_cbks.size();
		}
		trigger_event& trigger() { return m_evtTrigger; }

	protected:
		T					m_value;
		std::map<std::uintptr_t , on_value_changed>
							m_cbks;
		trigger_event		m_evtTrigger;
	};
}