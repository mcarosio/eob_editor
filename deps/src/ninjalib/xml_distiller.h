#pragma once

//#include <string>
//#include <vector>
//#include <list>
//#include <iostream>
//#include <sstream>

//#include "rapidxml/rapidxml.hpp"
//#include "rapidxml/rapidxml_print.hpp"
//#include "rapidxml/rapidxml_iterators.hpp"
//#include "rapidxml/rapidxml_utils.hpp"

#include "pugixml/pugiconfig.hpp"
#include "pugixml/pugixml.hpp"

#include "utils.h"

namespace utils
{
	class xml_distiller
	{
	public:

		using node = pugi::xml_node;
		using attribute = pugi::xml_attribute;
		using document = pugi::xml_document;
		using compiled_query = pugi::xpath_query;

		using query_filter = std::function<bool(const utils::xml_distiller::node& node)>;

		xml_distiller(void);
		virtual ~xml_distiller(void);

		void load(const std::experimental::filesystem::path& filePath);
		void load(const std::string& filePath);
		void load_xml(const std::string& xml);
		bool close(bool saveData=false);
		bool save();

		template <class _RetType>
		static utils::defaultable<_RetType> get_attribute_value(const node& elem, const std::string& name)
		{
			utils::defaultable<_RetType> value = utils::defaultable<_RetType>(_RetType());
			attribute attr = elem.attribute(name.c_str());

			_RetType v = utils::ascii_utils::parse<_RetType>(attr.value(), std::strlen(attr.value()), _RetType());
			value = v;
			return value;
		}

		template <>
		static utils::defaultable<std::string> get_attribute_value(const utils::xml_distiller::node& elem, const std::string& name)
		{
			utils::xml_distiller::attribute attr = elem.attribute(name.c_str());

			return utils::ascii_utils::parse<std::string>(attr.value(), std::strlen(attr.value()), "");
		}

		template <class _RetType>
		static utils::defaultable<_RetType> get_node_value(const utils::xml_distiller::node& elem)
		{
			return utils::ascii_utils::parse<_RetType>(elem.text().get(), std::strlen(elem.text().as_string()), _RetType());
		}

		template <>
		static utils::defaultable<std::string> get_node_value(const utils::xml_distiller::node& elem)
		{
			utils::defaultable<std::string> value("");
			value = utils::ascii_utils::parse<std::string>(elem.text().get(), std::strlen(elem.text().as_string()), value.default_value());
			return value;
		}

		template <class _InputType>
		static bool set_node_value(utils::xml_distiller::node& elem, const _InputType& value)
		{
			std::string text;
			utils::ascii_utils::write<_InputType>(text, value, ' ', ascii_utils::justify::left);
			return set_node_value(elem, text);
		}

		template <>
		static bool set_node_value(utils::xml_distiller::node& elem, const std::string& value)
		{
			if (elem.type() == pugi::xml_node_type::node_element)
			{
				auto nodeList = select_data_children(elem);
				if (nodeList.size() > 0)
				{
					return nodeList.front().set_value(value.c_str());
				}
				else
				{
					//false;
					auto newNode = elem.append_child(pugi::xml_node_type::node_pcdata);
					return newNode.set_value(value.c_str());
				}
			}
			else if (elem.type() == pugi::xml_node_type::node_pcdata || elem.type() == pugi::xml_node_type::node_cdata)
			{
				return elem.set_value(value.c_str());
			}
			return false;
		}

		template <class _InputType>
		static void add_attribute(utils::xml_distiller::node& elem, const std::string& name, const _InputType& value)
		{
			std::string text;
			utils::ascii_utils::write<_InputType>(text, value, ' ', ascii_utils::justify::left);
			auto attr = elem.append_attribute(name.c_str());
			attr.set_value(text.c_str());
		}

		template <>
		static void add_attribute(utils::xml_distiller::node& elem, const std::string& name, const std::string& value)
		{
			auto attr = elem.append_attribute(name.c_str());
			attr.set_value(value.c_str());
		}

		template<bool matchAll=true>
		std::list<utils::xml_distiller::node> query(const node& baseNode, const compiled_query& compiledQuery, const query_filter& filter=nullptr)
		{
			std::list<utils::xml_distiller::node> outputList;

			pugi::xpath_node_set out = compiledQuery.evaluate_node_set(baseNode);
			if (!matchAll && (!filter || filter(out.first().node())))
			{
				outputList.push_back(out.first().node());
				return outputList;
			}

			for (pugi::xpath_node node: out)
			{
				pugi::xml_node n = node.node();
				if (!filter || filter(n))
					outputList.push_back(n);
			}
			return outputList;
		}
		
		template<bool matchAll=true>
		std::list<node> query(const node& baseNode, const std::string& nodePath, const query_filter& filter=nullptr)
		{
			std::list<node> outputList;

			pugi::xpath_node_set out = baseNode.select_nodes(nodePath.c_str());
			if (!matchAll && (!filter || filter(out.first().node())))
			{
				outputList.push_back(out.first().node());
				return outputList;
			}

			for (pugi::xpath_node node: out)
			{
				pugi::xml_node n = node.node();
				if (!filter || filter(n))
					outputList.push_back(n);
			}
			return outputList;
		}
		
		template<bool matchAll=true>
		std::list<node> query(const std::string& nodePath, const query_filter& filter=nullptr)
		{
			return query<matchAll>(m_doc, nodePath, filter);
		}

		/*std::list<utils::xml_distiller::node> query(const node& baseNode, const std::string& nodePath, const query_filter& filter=nullptr);
		std::list<utils::xml_distiller::node> query(const std::string& nodePath, const query_filter& filter=nullptr);*/

		template <typename _ValueType, bool firstOnly = true>
		std::list<node> add_node(const std::string& xPath, const std::string &name, const _ValueType &value)
		{
			auto nodes = query(xPath);
			if (nodes.size() == 0)
				throw std::exception("Invalid query path specified");
			std::list<node> output;

			if (firstOnly)
			{
				auto n = nodes.front();
				auto xmlNode = add_node<_ValueType>(n, name, value);
				output.push_back(xmlNode);
			}
			else
			{
				for (auto n : nodes)
				{
					auto xmlNode = add_node<_ValueType>(n, name, value);
					output.push_back(xmlNode);
				}
			}

			return output;
		}

		template <typename _ValueType, bool firstOnly = true>
		node add_node(node& xmlNode, const std::string &name, const _ValueType &value)
		{
			auto parent = xmlNode.append_child(pugi::xml_node_type::node_element);
			parent.set_name(name.c_str());
			auto newNode = parent.append_child(pugi::xml_node_type::node_pcdata);
			set_node_value<_ValueType>(parent, value);

			return parent;
		}

		template <typename _ValueType, bool overwrite = true>
		static bool set_attribute(node& xmlNode, const std::string &name, const _ValueType &value)
		{
			auto attr = xmlNode.attribute(name.c_str());
			if (attr.empty())
			{
				attr = xmlNode.append_attribute(name.c_str());
			}
			return attr.set_value(value);
		}
		
		static bool remove_attribute(node& xmlNode, const std::string &name);

		template <bool firstOnly = true>
		bool remove_node(const std::string& xPath)
		{
			auto nodes = query(xPath);
			if (nodes.size() == 0)
				return false;

			if (firstOnly)
			{
				auto n = nodes.front();
				auto p = n.parent();
				p.remove_child(n);
			}
			else
			{
				for (auto n : nodes)
				{
					auto p = n.parent();
					p.remove_child(n);
				}
			}

			return true;
		}
		
		static int count_children(utils::xml_distiller::node& elem)
		{
			int count = 0;
			for (auto c : elem.children())
				++count;
			return count;
		}

	protected:
		pugi::xml_document			m_doc;
		std::string					m_filePath;

		static bool is_data_child(pugi::xml_node node)
		{
			return (node.type() == pugi::xml_node_type::node_pcdata || node.type() == pugi::xml_node_type::node_cdata);
		}

		static std::list<utils::xml_distiller::node> select_data_children(utils::xml_distiller::node& elem)
		{
			std::list<utils::xml_distiller::node> output;
			for (auto c : elem.children())
				if (is_data_child(c))
					output.push_back(c);
			return output;
		}
	};
}