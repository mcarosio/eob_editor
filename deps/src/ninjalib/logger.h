#pragma once

#include <string>
#include <queue>
#include <iostream>
#include <fstream>
#include <thread>
#include <functional>
#include <sstream>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <atomic>

namespace utils
{
	class logger
	{
	public:
		using log_callback = std::function<void (const std::string&)>;

		static const char* priority_names[];
		static const char* log_type_names[];
	
		enum class priority
		{
			none = 0,
			below_normal,
			normal,
			above_normal,
			high,
			critical
		};

		enum class log_type
		{
			info = 0,
			warning,
			error,
			debug
		};

		class message
		{		
			public:
				message(const std::string& msg, log_type t = log_type::info, priority p = priority::normal)
					: m_msg(msg), m_priorityLevel(p), m_type(t)
				{
				}
				message(const char *msg, size_t msgLength, log_type t = log_type::info, priority p = priority::normal)
					: m_msg(std::string(msg, msgLength)), m_priorityLevel(p), m_type(t)
				{
				}
				friend bool operator<(const logger::message& lhs, const logger::message& rhs)
				{
					return static_cast<int>(lhs.m_priorityLevel) < static_cast<int>(rhs.m_priorityLevel);
				}
				friend std::ostream& operator<< (std::ostream &out, const message &m)
				{
					out << "[" << m.get_type() << "|"
								<< m.get_priority() << "] "
								<< m.m_msg;
					return out;
				}
				logger::message& operator= (const char *text)
				{
					m_msg = text;
					m_priorityLevel = logger::priority::normal;
					m_type = logger::log_type::info;
					return *this;
				}
				logger::message& operator= (const std::string& text)
				{
					if (m_msg != text)
					{
						m_msg = text;
						m_priorityLevel = logger::priority::normal;
						m_type = logger::log_type::info;
					}
					return *this;
				}
				operator const char* ()
				{
					return this->m_msg.c_str();
				}

				const priority& get_priority_id() { return m_priorityLevel; };
				const log_type& get_type_id() { return m_type; };
				const std::string& get_message() const { m_msg; };
				size_t get_message_length() const { return m_msg.length(); };
				const char* get_priority() const { return priority_names[static_cast<int>(m_priorityLevel)]; };
				const char* get_type() const { return log_type_names[static_cast<int>(m_type)]; };

			protected:
				std::string			m_msg;
				priority			m_priorityLevel;
				log_type			m_type;

			private:
				message();
		};

		logger(void);
		logger(const std::string& filePath, bool logOnConsole = false, const std::string& timeFormat = "",
				const utils::logger::log_callback& logCbk = nullptr, const utils::logger::log_callback& errCbk = nullptr);
		virtual ~logger(void);

		void log_message(const std::string& msg, logger::log_type t = logger::log_type::info, logger::priority prio = logger::priority::normal);
		void log_message(const char *text, size_t textLength, logger::log_type t = logger::log_type::info, logger::priority prio = logger::priority::normal);
		void log_message(const logger::message& msg);
	
		template<typename T> 
		logger& operator<< (const T& data) 
		{
			m_textBuffer << data;
			if (m_textBuffer.str().rfind("\n") != std::string::npos)
			{
				logger::message m(m_textBuffer.str(), logger::log_type::info, logger::priority::normal);
				m_textBuffer.clear();
				m_textBuffer.str(std::string());
				log_message(m);
			}
			return (*this);
		}

	protected:
		std::priority_queue<logger::message>	m_msgQueue;
		std::ofstream							m_outStream;
		bool									m_logOnConsole;
		bool									m_logTimestamp;
		std::string								m_timeFormat;
		log_callback							m_errCbk;
		log_callback							m_logCbk;
		std::stringstream						m_textBuffer;
		std::atomic<bool>						m_continue;

		std::mutex								m_logMutex;
		std::condition_variable					m_evtDataReady;
		std::thread								m_messageSpooler;
	
		void spool_messages();
	};
}